import React, {Component} from 'react';

class Construindo extends Component{
    constructor(props){
        super(props);
        
        this.state={};
    }

    componentDidMount()
    {

    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = prevProps.geral;
        const {geral} = props;
    }

    render()
    {
        const {state, props} = this;

        return(
            <div id="bloco">
                <h1>Construindo</h1>
                <div class="caixaTexto alinhadoEsquerda">
                    <p>Infelizmente essa seção ainda está passando por construções nos reinos etéreos e está instável demais para ser visualizada</p>
                    <p>Por favor, tenha um pouco mais de paciência...</p>
                </div>
            </div>
        );
    }
}

export default Construindo;
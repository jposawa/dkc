import React, {Component} from 'react';
import './styles.css';

class Fichas extends Component{
    constructor(props){
        super(props);
        // this.mesaEscolhida.value = undefined;
        this.state={
            opOver:undefined,
            painelOver:false,
            // modulo:"PRINCIPAL",
            atributoEscolhido:"fisico",
            apareceSalvar:false,
            mostraModulos:true,
            rolagem:{
                tipoCompetencia:false,
                atributo:"fisico",
                competencia:0,
                bonus:0,
                dificuldade:50,
                mesaEscolhida:undefined,
            },
        };
    }

    componentDidMount()
    {
        const {props} = this;
        const {geral} = props;

        props.PuxaFichas();
        props.PuxaDados(geral.modulo, "competencias");
        props.PuxaDados(geral.modulo, "tracos");
        props.PuxaDados(geral.modulo, "equipamentos");
        props.PuxaMesas();
        props.PuxaRacas();
        // this.CompetenciasRolagem("fisico");
        // this.ParametrosRolagem("rolAtributo")
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = {...prevProps.geral};
        const {geral} = props;

        if(prevState !== null && prevState !== undefined)
        {
            if(prevGeral.modulo !== geral.modulo)
            {
                // console.log("xablau");
                props.PuxaFichas();
                props.PuxaDados(geral.modulo, "competencias");
                props.PuxaDados(geral.modulo, "tracos");
                props.PuxaDados(geral.modulo, "itens");
                props.PuxaDados(geral.modulo, "racas");
                props.PuxaRacas();
            }
            if(prevState.atributoEscolhido !== state.atributoEscolhido)
            {
                // console.log("plow");
                this.RenderizaDados();
            }
            // console.log(JSON.stringify(state.fichaAberta));
            // console.log(JSON.stringify(state.fichaDetalhada))

            // console.log(prevState);

            if(state.fichaAberta !== undefined)
            {
                let apareceSalvar = this.ComparaFichas();

                const campoRolCompetencias = document.getElementById("rolCompetencia");

                // console.log(campoRolCompetencias.innerHTML === "");
                if(campoRolCompetencias !== null && campoRolCompetencias !== undefined)
                {
                    if(campoRolCompetencias.innerHTML === "" || (prevState.rolagem.atributo !== state.rolagem.atributo))
                    {
                        this.CompetenciasRolagem(state.rolagem.atributo);
                    }
                }

                // this.CompetenciasRolagem(state.rolagem.atributo);

                /* if(prevState.fichaAberta !== undefined)
                {
                    if(prevState.fichaAberta.id !== state.fichaAberta.id)
                    {
                        this.ParametrosRolagem("rolAtributo", state.fichaAberta.atributos.fisico[0]);
                    }
                }
                else
                {
                    this.ParametrosRolagem("rolAtributo", state.fichaAberta.atributos.fisico[0]);
                } */

                // console.log(apareceSalvar && state.opOver !== "detalhaFicha");
                if(prevState.apareceSalvar !== apareceSalvar)
                {
                    this.setState({apareceSalvar:apareceSalvar});
                }
                if(state.apareceSalvar && state.opOver !== "detalhaFicha")
                {
                    this.setState({apareceSalvar:false});
                }
            }
            /* if(prevState.atributoEscolhido === undefined && geral.competencias !== undefined)
            {
                this.setState({atributoEscolhido:"fisico"});
            } */
        }

        if(this.state.opOver === "criaFicha")
        {
            if(this.state.opOver !== prevState.opOver)
            {
                const campoOpcoesModulos = document.getElementById("listaModulos");
                // console.log(campoOpcoesModulos.innerHTML === "");
                if(campoOpcoesModulos !== null && campoOpcoesModulos !== undefined)
                {
                    if(campoOpcoesModulos.innerHTML === null || campoOpcoesModulos.innerHTML === undefined || campoOpcoesModulos.innerHTML === "")
                    {
                        this.OpcoesModulos();
                    }
                }
                if(this.mesaEscolhida !== null && this.mesaEscolhida !== undefined)
                {
                    if(state.mesaEscolhida === null || state.mesaEscolhida === undefined || state.mesaEscolhida === "")
                    {
                        this.MostraMesas();
                    }
                    else if(state.mesaEscolhida === "SM" && prevState.mesaEscolhida !== state.mesaEscolhida)
                    {
                        this.OpcoesModulos();
                    }
                }
            }
            else
            {
                if(state.mesaEscolhida === "SM" && prevState.mesaEscolhida !== state.mesaEscolhida)
                {
                    this.OpcoesModulos();
                }
                if(geral.listaRacas !== prevGeral.listaRacas)
                {
                    // props.PuxaRacas();
                    this.OpcoesRaca();
                }
            }
        }

        /* if(state.apareceSalvar && state.fichaAberta !== undefined)
        {
            props.SalvaFicha({...state.fichaAberta});
            this.setState({apareceSalvar:false});
        } */
    }

    ParametrosRolagem = (parametro,valor) =>
    {
        const {rolagem} = this.state;
        const {fichaAberta} = this.state;
        const competenciasDisponiveis = [];
        let nomeParametro = "";
        let campo;
        // console.log(parametro.target);
        if(typeof parametro === "object")
        {
            campo = parametro.target;

            nomeParametro = campo.id;
            valor = campo.value;
        }
        else if(parametro === undefined || parametro === null || parametro === "")
        {
            nomeParametro = "rolAtributo";
            rolagem.tipoCompetencia = false;
            rolagem.dificuldade = 50;
        }
        else
        {
            nomeParametro = parametro;
        }
        
        // console.log(typeof parametro);
        
        switch(nomeParametro)
        {
            case "tipoCompetencia":
                // console.log(campo.checked);
                if(campo !== null && campo !== undefined)
                {
                    valor = campo.checked;
                }
                rolagem.tipoCompetencia = valor;
            break;

            case "rolAtributo":
                rolagem.atributo = valor;
                rolagem.competencia = 0;
                this.CompetenciasRolagem(valor);
            break;

            case "rolCompetencia":
                rolagem.competencia = this.ValorPadrao(valor,1);
            break;

            case "rolBonus":
                rolagem.bonus = this.ValorPadrao(valor);
            break;

            case "rolDificuldade":
                rolagem.dificuldade = this.ValorPadrao(valor,50);
            break;
        }

        if(rolagem.tipoCompetencia)
        {
            /* let opcoesCompetencia = "";
            const campoCompetencia = document.getElementById("rolCompetencia");

            fichaAberta.atributos[rolagem.atributo][1].forEach(competencia => {
                opcoesCompetencia += "<option value\"" + competencia.
            }); */
        }

        this.setState({rolagem:rolagem});
        // console.log(campo.id);
    }

    ValorPadrao = (valor, padrao) =>
    {
        padrao = parseInt(padrao);

        if(isNaN(padrao) || padrao === null)
        {
            padrao = 0;
        }

        valor = parseInt(valor);

        if(isNaN(valor) || valor === null)
        {
            valor = padrao;
        }

        return valor;
    }

    CompetenciasRolagem = (atributo) =>
    {
        const {state,props} = this;
        const {geral} = props;
        const {competencias} = geral;
        const {fichaAberta, rolagem} = state;

        const campo = document.getElementById("rolCompetencia");
        if(campo !== null && campo !== undefined)
        {
            let opcoes = "<option value=\"0\">Apenas atributo</option>";

            competencias[rolagem.atributo].forEach(competencia => {
                let nivel;
                let competenciasJogador = fichaAberta.atributos[rolagem.atributo][1];
                if(competenciasJogador === undefined || competenciasJogador === null)
                {
                    nivel = 0
                }
                else
                {
                    nivel = fichaAberta.atributos[rolagem.atributo][1][competencia.nome];
                }

                if(nivel === undefined || nivel === null || nivel === "")
                {
                    nivel = 0;
                }
                opcoes += "<option value=\""+nivel+"\">";
                opcoes += competencia.nome;
                opcoes += "</option>";
            })

            campo.innerHTML = opcoes;
        }
    }

    NivelCompetencia = () =>
    {
        const {props,state} = this;
        const {geral} = props;
        const {fichaAberta, rolagem} = state;
        const {competencias} = geral;

        let nivel;

        if(fichaAberta.atributos[rolagem.atributo].length > 1)
        {
            nivel = fichaAberta.atributos[rolagem.atributo][1][rolagem.competencia];
        }

        nivel = this.ValorPadrao(nivel,0);

        return nivel;
    }

    ExecutaRolagem = evento =>
    {
        const {rolagem, fichaAberta} = this.state;
        const valorDado = Math.floor(Math.random() * 100) + 1;
        let valorAtributo = this.ValorPadrao(fichaAberta.atributos[rolagem.atributo][0],1);
        let pontos = valorAtributo + rolagem.bonus;
        let mensagem = "";
        if(rolagem.tipoCompetencia)
        {
            pontos += rolagem.competencia;
        }

        let resultado = valorDado + pontos - rolagem.dificuldade;

        // console.log(resultado);

        if(resultado >= 0)
        {
            mensagem = "<b class=\"corSucesso\">" + (valorDado + pontos) + " (SUCESSO)</b>";
        }
        else
        {
            mensagem = "<b class=\"corFalha\">" + (valorDado + pontos) + " (Falha)</b>";
        }

        // mensagem += "Sua rolagem somou " + (valorDado + pontos);

        let campoResultado = document.getElementById("resultado");

        campoResultado.innerHTML = mensagem;
    }

    PontosDisponiveis = () =>
    {
        const {state,props} = this;
        const {geral} = props;
        const {fichaAberta} = state;

        fichaAberta.pontosDisponiveis = fichaAberta.pontosTotais - fichaAberta.pontosGastos;

        this.setState({fichaAberta:fichaAberta});
    }

    OpcoesModulos()
    {
        const opcoesModulos = document.getElementById("listaModulos");
        let htmlModulos = "";
        this.props.geral.listaModulos.forEach(modulo => {
            htmlModulos += "<option value=\""+modulo+"\">";
            htmlModulos += modulo;
            htmlModulos += "</option>";
        });

        opcoesModulos.innerHTML = htmlModulos;
    }

    ComparaFichas = () =>
    {
        const {state, props} = this;
        const {fichaAberta} = state;
        const {geral} = props;

        const ficha = geral.listaFichas.filter(lf => (lf.id === fichaAberta.id))[0];

        // console.log(ficha);
        // console.log(fichaAberta);

        return(JSON.stringify(ficha) !== JSON.stringify(fichaAberta));

        // return false;
    }

    CliqueOverlay = (evento) =>
    {
        // console.log(evento.target);
        if(evento.target.id === "slotPainelOver")
        {
            this.ChamaOp();
        }
    }

    ChamaOp = (operacao) =>
    {
        let painelOver = false;
        if(operacao !== undefined && operacao !== null)
        {
            operacao = operacao.trim();
            if(operacao !== "")
            {
                painelOver = true;
            }
        }

        this.setState({opOver:operacao, painelOver:painelOver});
    }

    CriaFicha = (evento, nomePersonagem, pontosIniciais, raca, mesa) =>
    {
        // console.log("entrou");
        //TRATANDO DADOS
        evento.preventDefault();
        // console.log(evento);
        const ficha = {};
        const {listaMesas} = this.props.geral;
        nomePersonagem = nomePersonagem.trim();
        if(nomePersonagem !== undefined && nomePersonagem !== "")
        {
            pontosIniciais = pontosIniciais.trim();
            if(pontosIniciais !== undefined && pontosIniciais !== "")
            {
                pontosIniciais = parseInt(pontosIniciais);
                if(isNaN(pontosIniciais) || pontosIniciais < 1)
                {
                    pontosIniciais = 100;
                }
                if(mesa !== undefined)
                {
                    mesa = mesa.trim();
                }
                if(mesa === undefined || mesa === null)
                {
                    mesa = "SM";
                }
            }
            else
            {
                alert("Você precisa informar quantos pontos a ficha possui inicialmente");
                this.pontosIniciais.focus();
                return false;
            }
        }
        else
        {
            alert("Você precisa dar um nome para o personagem");
            this.nomePersonagem.focus();
            return false;
        }
        //TRATANDO DADOS

        ficha.nome = nomePersonagem;
        ficha.raca = raca;
        ficha.pontosTotais = pontosIniciais;
        ficha.pontosGastos = 0;
        ficha.pontosDisponiveis = pontosIniciais;
        ficha.mesa = mesa;
        ficha.modulo = this.props.geral.modulo;
        // ficha.usuario = this.props.usuario.id;
        ficha.atributos = {
            fisico:[5],
            coordenacao:[5],
            inteligencia:[5],
            astucia:[5],
            vontade:[5],
            presenca:[5],
        };
        // ficha.recemCriada = true;

        // console.log(this.props.SalvaFicha(ficha));
        this.props.SalvaFicha(ficha)
        // this.setState({opOver:undefined});
        this.DetalhaFicha(ficha);
        // console.log(ficha);
    }

    DetalhaFicha = ficha =>
    {
        if(ficha !== undefined && ficha !== null)
        {
            // console.log(ficha);
            this.ChamaOp("detalhaFicha");
            this.props.AlteraModulo(ficha.modulo);
            this.setState({
                fichaDetalhada:{...ficha},
                fichaAberta: {...ficha},
                // modulo:ficha.modulo,
            });
        }
    }

    AlternaSecao = (evento, nomeSecao) =>
    {
        const campo = evento.target;
        // console.log(evento.target);
        if(nomeSecao !== null && nomeSecao !== undefined)
        {
            nomeSecao = nomeSecao.trim();
            if(nomeSecao !== "")
            {
                let listaSecao = document.getElementsByName(nomeSecao);

                if(listaSecao.length > 0)
                {
                    let secao = listaSecao[0];
                    if(!secao.classList.toggle("fechado"))
                    {
                        if(nomeSecao === "competencias")
                        {
                            console.log("pew");
                            this.RenderizaDados();
                        }
                    }
                    campo.classList.toggle("cabecalhoAtivo");
                }
            }
        }
    }

    RenderizaDados = (dados) =>
    {
        // console.log("entrou");
        if(dados === undefined || dados === null || dados === "")
        {
            dados = "competencias";
        }

        let opcoes = "";
        const {competencias, tracos, itens} = this.props.geral;
        
        console.log(dados);

        switch(dados)
        {
            case "competencias":    
                const {atributoEscolhido} = this.state;
                // console.log(atributoEscolhido);
                // console.log(competencias[atributoEscolhido] !== undefined && competencias[atributoEscolhido] !== null);
                if(competencias[atributoEscolhido] !== undefined && competencias[atributoEscolhido] !== null)
                {
                    const totalCompetencias = competencias[atributoEscolhido].sort((a,b) => {
                        if(a.nome < b.nome)
                        {
                            return -1;
                        }
                        if(a.nome > b.nome)
                        {
                            return 1;
                        }

                        return 0;
                    });
                    totalCompetencias.forEach(competencia => {
                        opcoes += "<option value=\""+competencia.nome+"\">"+competencia.nome+" ["+competencia.custo+"]</option>";
                    });
                    // console.log(this.opCompetencias);
                    // console.log(opcoes);
                    this.opCompetencias.innerHTML = opcoes;
                }
            break;

            case "tracos":
                if(tracos !== null && tracos !== undefined)
                {
                    tracos.forEach(traco => {
                        // console.log(traco);
                        opcoes += "<option value = \"" + traco.nome + "\">"+traco.nome + " ["+traco.custo+"]</option>";            
                    })
            
                    this.opTracos.innerHTML = opcoes;
                }
            break;

            default:
                console.log("Entrou em nenhum");
            break;
        }
    }

    AlteraFicha = (evento) =>
    {
        const atributos = ["fisico","astucia","coordenacao","inteligencia","presenca","vontade"];
        const {state,props} = this;
        const {fichaAberta} = state;
        const campo = evento.target;
        // console.log(campo.name);
        const chaves = campo.name.split("@");

        const custoAtributo = 5;

        const limite = {
            atributo: 20,
            competencia:20
        }
        // console.log(chaves);

        let valor = parseInt(campo.value);

        if(isNaN(valor) || valor < 0)
        {
            valor = 0;
        }

        if(chaves.length > 1)
        {
            if(atributos.includes(chaves[0]))
            {

                if(valor > limite.competencia)
                {
                    valor = limite.competencia;
                    campo.value = valor;
                }
                let nvAnterior = fichaAberta.atributos[chaves[0]][1][chaves[1]];
                

                const custo = this.PegaCustoCompetencia(chaves[1]);

                console.log(custo);
                fichaAberta.pontosGastos += (valor - nvAnterior) * custo;
                
                if(valor === 0 || valor === "")
                {
                    valor = null;
                }
                fichaAberta.atributos[chaves[0]][1][chaves[1]] = valor;
            }
            else if(chaves[0] === "ferimentos")
            {
                fichaAberta.ferimentos[chaves[1]] = valor;
            }
            // this.setState({fichaAberta:fichaAberta});
        }
        else
        {
            if(atributos.includes(chaves[0]))
            {
                let valorAnterior = fichaAberta.atributos[chaves[0]][0];

                if(valor > limite.atributo)
                {
                    valor = limite.atributo;
                    campo.value = valor;
                }
                fichaAberta.atributos[chaves[0]][0] = valor;
                fichaAberta.pontosGastos += (valor - valorAnterior) * 5;
            }
            else
            {
                console.log(chaves[0]);
            }
        }
        // console.log(campo.value);
        this.setState({fichaAberta:fichaAberta});
    }

    AlteraNomeFicha = () =>{
        const {state} = this;
        const {fichaAberta} = state;
        const novoNome = window.prompt("Informe o novo nome da personagem");
        let _erro = false;

        if(novoNome === null || novoNome === undefined)
        {
            _erro = true;
        }
        else if(novoNome.trim() === "")
        {
            _erro = true;
        }

        if(_erro)
        {
            alert("O novo nome não pode ser em branco");
        }
        else
        {
            fichaAberta.nome = novoNome;
            this.setState({fichaAberta:fichaAberta});
        }
    }

    AlteraImagemFicha = () =>{
        const {state} = this;
        const {fichaAberta} = state;
        let novaUrl = window.prompt("Insira URL da imagem do personagem");

        if(novaUrl === null || novaUrl === undefined)
        {
            novaUrl = "https://www.festivalclaca.cat/imgfv/b/86-866911_circle-icons-profile-profile-picture-circle-png.png";
        }
        else if(novaUrl.trim() === "")
        {
            novaUrl = "https://www.festivalclaca.cat/imgfv/b/86-866911_circle-icons-profile-profile-picture-circle-png.png";
        }

        fichaAberta.imagem = novaUrl;
        this.setState({fichaAberta:fichaAberta});
    }

    RemoveDado = (tipoDado, nomeDado) =>
    {
        const confirma = window.confirm("Deseja mesmo remover?");
        if(confirma)
        {
            const {state,props} = this;
            const {geral} = props;
            const {fichaAberta} = state;
            const indice = fichaAberta[tipoDado].findIndex(fad => (fad.nome === nomeDado));

            if(tipoDado === "tracos")
            {
                const custo = fichaAberta[tipoDado][indice].custo;
                fichaAberta.pontosGastos += custo;
            }

            fichaAberta[tipoDado].splice(indice,1);

            this.setState({fichaAberta:fichaAberta});
        }
    }

    MostraInfo = (info) =>
    {
        if(info !== undefined && info !== null && info !== "")
        {
            alert("DESCRIÇÃO:\n" + info);
        }
    }

    ExcluiFicha = (e) =>
    {
        const {fichaAberta} = this.state;

        const confirma = window.confirm("Você tem certeza? A ação é irreversível");

        if(confirma)
        {
            fichaAberta.exclui = true;
            this.setState({
                opOver:undefined,
                painelOver:false,
            });
            this.props.SalvaFicha(fichaAberta);
        }
    }

    AbreFormInterno = (botao, nomeForm) =>
    {
        const form = document.getElementById(nomeForm);
        const nomeDados = nomeForm.substring(4, nomeForm.length).toLowerCase();
        // const botao = evento.target;

        form.classList.toggle("fechado");
        botao.classList.toggle("btnFechaAdd");

        this.RenderizaDados(nomeDados);
        // this.setState({atributoEscolhido:"fisico"});
    }

    PegaCustoCompetencia = (competencia) =>
    {
        const {state, props} = this;
        const {competencias} = props.geral;
        const listaCompetencias = Object.values(competencias);
        let contador = 0;
        let encontrou = false;
        let filtro;
        let competenciaEscolhida;

        while(!encontrou && contador < 6)
        {
            filtro = listaCompetencias[contador++].filter(c => (c.nome === competencia));

            if(filtro.length > 0)
            {
                encontrou = true;
                competenciaEscolhida = {...filtro[0]};
            }
        }

        // console.log(filtro[0]);
        // console.log(competenciaEscolhida);
        if(competenciaEscolhida !== undefined)
        {
            return(competenciaEscolhida.custo);
        }
        else
        {
            return(0);
        }
    }

    AddTraco = (evento) =>
    {
        evento.preventDefault();
        const {state,props} = this;
        const {atributoEscolhido, fichaAberta} = state;
        const {geral} = props;
        const {tracos} = geral;

        // let valor = parseInt(this.nvComp.value);

        if(fichaAberta.tracos === undefined)
        {
            fichaAberta.tracos = [];
        }

        const tracosIguais = fichaAberta.tracos.filter(tfa => (tfa.nome === this.opTracos.value));

        if(tracosIguais.length > 0)
        {
            // procede = window.confirm("Sua ficha já possui essa competência, deseja substituir o valor?");

            alert("Sua ficha já possui esse traço!");
        }
        else
        {
            const indice = tracos.findIndex(traco => (traco.nome === this.opTracos.value));

            fichaAberta.tracos.push(tracos[indice]);

            const custo = tracos[indice].custo;

            fichaAberta.pontosGastos -= custo;

            const botao = document.getElementsByClassName("btnFechaAdd")[0];

            this.AbreFormInterno(botao, "formTracos");
            
            this.setState({fichaAberta:fichaAberta});
        }
    }

    AddCompetencia = (evento) =>
    {
        evento.preventDefault();
        const {state,props} = this;
        const {atributoEscolhido, fichaAberta} = state;
        let nvAnterior;
        let procede = false;
        let valor = parseInt(this.nvComp.value);

        if(isNaN(valor))
        {
            valor = 0;
        }
        if(fichaAberta.atributos[atributoEscolhido][1] === undefined)
        {
            fichaAberta.atributos[atributoEscolhido][1] = {};
        }
        else
        {
            nvAnterior = fichaAberta.atributos[atributoEscolhido][1][this.opCompetencias.value];
        }

        if(nvAnterior !== undefined)
        {
            // procede = window.confirm("Sua ficha já possui essa competência, deseja substituir o valor?");

            alert("Sua ficha já possui essa competência");
        }
        else
        {
            procede = true;
            nvAnterior = 0;
        }
        // console.log(nvAnterior);
        if(procede)
        {
            fichaAberta.atributos[atributoEscolhido][1][this.opCompetencias.value] = valor;

            const custo = this.PegaCustoCompetencia(this.opCompetencias.value);

            console.log(nvAnterior);
            fichaAberta.pontosGastos += (valor - nvAnterior) * custo;

            const botao = document.getElementsByClassName("btnFechaAdd")[0];

            this.AbreFormInterno(botao, "formCompetencias");
            
            this.setState({fichaAberta:fichaAberta});
        }
        else
        {
            this.nvComp.focus();// = nvAnterior;
        }
    }

    AddItem = (evento) =>
    {
        evento.preventDefault();
        const {state,props} = this;
        const {atributoEscolhido, fichaAberta} = state;
        const {geral} = props;
        const {itens} = geral;

        // let valor = parseInt(this.nvComp.value);

        if(fichaAberta.itens === undefined)
        {
            fichaAberta.itens = [];
        }

        const itensIguais = fichaAberta.itens.filter(tfa => (tfa.nome === this.opItens.value));

        if(itensIguais.length > 0)
        {
            // procede = window.confirm("Sua ficha já possui essa competência, deseja substituir o valor?");

            alert("Sua ficha já possui esse item!");
        }
        else
        {
            // const indice = tracos.findIndex(traco => (traco.nome === this.opTracos.value));
            const item = {
                nome:this.opItens.value,
            };
            let qtdItem = prompt("Quantas unidades de " + item.nome + " você tem?",1);

            qtdItem = parseInt(qtdItem);

            if(isNaN(qtdItem) || qtdItem < 1)
            {
                qtdItem = 1;
            }

            if(window.confirm("Deseja adicionar uma descrição ao item?"))
            {
                let descricao = prompt("Escreva uma descrição curta");

                item.descricao = descricao.trim();
            }

            item.quantidade = qtdItem;

            fichaAberta.itens.push(item);

            alert("Lembre de reduzir o dinheiro de seu inventário");

            const botao = document.getElementsByClassName("btnFechaAdd")[0];

            this.AbreFormInterno(botao, "formItens");
            
            this.setState({fichaAberta:fichaAberta});
        }
    }

    AlteraModulo = (evento) =>
    {
        const {modulo} = this.props.geral;
        const opcao = evento.target;
        // console.log(typeof evento);

        // console.log(opcao);

        if(opcao.value !== modulo)
        {
            // this.setState({modulo:opcao.value});
            this.props.AlteraModulo(opcao.value);
        }
    }

    MostraMesas = (ficha) =>
    {
        const {state,props} = this;
        const {geral} = props;
        const {listaMesas} = geral;

        let opcoes = "<option value = \"SM\">Sem mesa</option>";
        // console.log(listaMesas);

        Object.values(listaMesas).forEach(mesa => {
            opcoes += "<option value = \"" + mesa.id + "\">";
            opcoes += mesa.nome;
            opcoes += "</option>";
        })

        if(this.mesaEscolhida !== null && this.mesaEscolhida !== undefined)
        {
            this.mesaEscolhida.innerHTML = opcoes;
        }
    }

    SelecionaMesa = (evento) =>
    {
        const {props} = this;
        const {listaMesas} = props.geral;
        const campo = evento.target;
        let mesaEscolhida = campo.value;
        // console.log(campo);
        // console.log(mesaEscolhida);

        if(mesaEscolhida === undefined || mesaEscolhida === "" || mesaEscolhida.toLowerCase() === "sm" || mesaEscolhida === null)
        {
            mesaEscolhida = "SM";
            props.AlteraModulo();

        }
        else
        { 
            props.AlteraModulo(listaMesas[mesaEscolhida].modulo);
        }

        this.setState({mesaEscolhida:mesaEscolhida});
    }

    OpcoesRaca = () =>
    {
        const {state, props} = this;
        const {geral} = props;
        const {listaRacas} = geral;
        let opcoes = "";

        // console.log(listaRacas);

        listaRacas.forEach(raca => {
            console.log(raca);
            opcoes += "<option>";
            opcoes += raca.nome;
            opcoes += "</option>";
        })

        this.racaPersonagem.innerHTML = opcoes;
    }

    MandaSalvaFicha = (ficha) =>{
        this.props.SalvaFicha({...ficha});

        this.setState({apareceSalvar:false});
    }

    render()
    {
        const {state, props} = this;
        const {geral} = props;
        const {listaFichas} = geral;
        const {fichaAberta, atributoEscolhido} = state;

        if(fichaAberta !== null && fichaAberta !== undefined)
        {
            if(fichaAberta.ferimentos === null || fichaAberta.ferimentos === undefined)
            {
                fichaAberta.ferimentos = [0,0,0,0,0];
            }

            if(fichaAberta.imagem === null || fichaAberta.imagem === undefined)
            {
                fichaAberta.imagem = "https://www.festivalclaca.cat/imgfv/b/86-866911_circle-icons-profile-profile-picture-circle-png.png";
            }
        }

        return(
            <div id="bloco">
                <div>
                    <h1>Fichas</h1>
                    <button type="button" onClick={() => {this.ChamaOp("criaFicha")}} className="btnConfirma">+</button>
                    {(listaFichas !== undefined && listaFichas !== null) ?
                    <ul id="listaFichas" className="listaCards">
                        {listaFichas.map(ficha => {
                            if(ficha.id !== "" && ficha.id !== undefined)
                            {
                                {/* console.log(ficha); */}
                                return(
                                    <li key={ficha.id} onClick = {() => {this.DetalhaFicha({...ficha})}}>
                                        <div>
                                            <p><b>{ficha.nome}</b></p>
                                            <p><b>Raça</b>:&nbsp; {ficha.raca}</p>
                                            <p><b>Pontos totais</b>:&nbsp; {ficha.pontosTotais}</p>
                                        </div>
                                        <div className="resumoAtributos c2">
                                            <div className="b1">
                                                <p><b>F</b> [{ficha.atributos.fisico[0]}]</p>
                                                <p><b>I</b> [{ficha.atributos.inteligencia[0]}]</p>
                                                <p><b>V</b> [{ficha.atributos.vontade[0]}]</p>
                                            </div>
                                            <div className="b2">
                                                <p><b>C</b> [{ficha.atributos.coordenacao[0]}]</p>
                                                <p><b>A</b> [{ficha.atributos.astucia[0]}]</p>
                                                <p><b>P</b> [{ficha.atributos.presenca[0]}]</p>
                                            </div>
                                        </div>
                                    </li>
                                );
                            }
                        })}
                    </ul>
                    : undefined
                    }
                </div>
                <div id="slotPainelOver" style={(state.opOver === null || state.opOver === undefined || state.opOver === "") ? {opacity:0,visibility:"hidden"} : {visibility:"visible",opacity:1}} onClick = {this.CliqueOverlay}>
                    <div id="painelOver" className={state.opOver === "criaFicha" ? "painelRecuado" : undefined}>
                        <button type="button" className="btn btnFechaOver" onClick={() => this.ChamaOp()}>&times;</button>
                        <div>
                        {
                            {
                                'criaFicha':
                                <>
                                <h3>Criação ficha</h3>
                                <p>
                                    Nome:&nbsp;
                                    <input type="text" placeholder="Nome personagem" ref={(r) => {this.nomePersonagem = r}} autoFocus={true}/>
                                </p>

                                <p>
                                    Raça:&nbsp;
                                    <select ref={r => this.racaPersonagem = r}>
                                        <option value="Humano">Humano</option>
                                        <option value="Elfo">Elfo</option>
                                        <option value="Anão">Anão</option>
                                        <option value="Faelir">Faelir</option>
                                        <option value="Siruna">Siruna</option>
                                        <option value="Ravenfir">Ravenfir</option>
                                        <option value="Sintético">Sintético</option>
                                        <option value="Outro">Outro</option>
                                    </select>
                                </p>
                                <p>
                                    Pontos iniciais:&nbsp;
                                    <input type="number"  defaultValue="100" ref={(r) => this.pontosIniciais = r}/>
                                </p>
                                <p>
                                    Mesa:&nbsp;
                                    <select id="opcoesMesa" onChange={this.SelecionaMesa} ref={(r) => this.mesaEscolhida = r}/>
                                </p>
                                {
                                    state.mesaEscolhida === null || state.mesaEscolhida === undefined || state.mesaEscolhida === "SM" ?
                                    <>
                                    <p>
                                        Módulo: &nbsp; 
                                        <select id="listaModulos" onChange={this.AlteraModulo}/>
                                    </p>
                                    </>:undefined
                                }
                                <p>
                                    <button className="btn btnConfirma" onClick = {e => this.CriaFicha(e, this.nomePersonagem.value, this.pontosIniciais.value, this.racaPersonagem.value, this.mesaEscolhida.value)}>Criar</button>
                                </p>
                                </>,

                                'detalhaFicha':
                                <>
                                {fichaAberta !== undefined ?
                                <>
                                <div id="cabecalhoFicha">
                                    <div className="slotImagem">
                                        <img src={fichaAberta.imagem}/>
                                        <button className="btnPequeno" style={fichaAberta.jogador.id === props.usuario.id ? undefined : {"display":"none"}} onClick= {() => {this.AlteraImagemFicha()}}>&#9998;</button>
                                    </div>
                                    <div className="infosCabecalho">
                                        <h3>
                                            {fichaAberta.nome}
                                            &nbsp;<button className="btnPequeno" style={fichaAberta.jogador.id === props.usuario.id ? undefined : {"display":"none"}} onClick= {() => {this.AlteraNomeFicha()}}>&#9998;</button>
                                        </h3>
                                        <p>   
                                            [{fichaAberta.raca}]
                                        </p>
                                        <p><b>Mesa: </b>{geral.listaMesas[fichaAberta.mesa].nome} ({fichaAberta.modulo})</p>
                                        <h4>
                                            Pontos:&nbsp;
                                            <span className="txtTituloMenor">
                                                <span className={fichaAberta.pontosTotais < fichaAberta.pontosGastos ? "txtAlerta" : undefined}>
                                                {fichaAberta.pontosGastos} |&nbsp;
                                                </span>
                                                {fichaAberta.pontosTotais}
                                            </span>
                                        </h4>
                                    </div>
                                </div>
                                <div id="corpo">
                                    <h4 className="cabecalhoAtivo" onClick={(e) => this.AlternaSecao(e,"atributos")}>ATRIBUTOS</h4>
                                    <div name="atributos" className="bloco c2">
                                        <div className="b1">
                                            <p><b>Físico</b>:&nbsp; <input name="fisico" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.fisico[0]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                            <p><b>Inteligência</b>:&nbsp; <input name="inteligencia" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.inteligencia[0]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                            <p><b>Vontade</b>:&nbsp; <input name="vontade" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.vontade[0]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                        </div>
                                        <div className="b2">
                                            <p><b>Coordenação</b>:&nbsp; <input name="coordenacao" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.coordenacao[0]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                            <p><b>Astúcia</b>:&nbsp; <input name="astucia" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.astucia[0]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                            <p><b>Presença</b>:&nbsp; <input name="presenca" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.presenca[0]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                        </div>
                                    </div>

                                    <h4 onClick={(e) => this.AlternaSecao(e,"tracos")}>TRAÇOS</h4>
                                    <div name="tracos" className="bloco c1 fechado">
                                        <p><button type="button" className="btnRedondo btnConfirma" onClick={(e) => this.AbreFormInterno(e.target,"formTracos")}>+</button></p>
                                        <div>
                                            <form id="formTracos" className="formInterno fechado">
                                                <p>
                                                    <select ref={(r) => (this.opTracos = r)}>
                                                    </select>
                                                    &nbsp;
                                                    <button className="btnConfirma" onClick={(e) => this.AddTraco(e)}>&#10003;</button>
                                                </p>
                                            </form>
                                            {
                                            fichaAberta.tracos !== undefined && fichaAberta.tracos.length > 0 ?
                                            <>
                                            {
                                                fichaAberta.tracos.map(traco => (
                                                    <>
                                                    <p key={fichaAberta.id + traco.nome}>
                                                        <b onClick={() => {this.MostraInfo(traco.descricao)}}><u>{traco.nome}</u></b> <button className="btnRemover" type="button" onClick={(e) => this.RemoveDado("tracos",traco.nome)}>&times;</button>
                                                    </p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                            }
                                        </div>
                                    </div>

                                    <h4 onClick={(e) => this.AlternaSecao(e,"competencias")}>COMPETÊNCIAS</h4>
                                    <div name="competencias" className="bloco c1 fechado">
                                        <p><button type="button" className="btnRedondo btnConfirma" onClick={(e) => this.AbreFormInterno(e.target,"formCompetencias")}>+</button></p>
                                        <div>
                                            <form id="formCompetencias" className="formInterno fechado">
                                                <p>
                                                    <select name="atributos" onChange={(e) => {this.setState({atributoEscolhido:e.target.value})}}>
                                                        <option value="fisico">Físico</option>
                                                        <option value="coordenacao">Coordenação</option>
                                                        <option value="inteligencia">Inteligência</option>
                                                        <option value="astucia">Astúcia</option>
                                                        <option value="vontade">Vontade</option>
                                                        <option value="presenca">Presença</option>
                                                    </select>
                                                    <select ref={(r) => (this.opCompetencias = r)}>
                                                    </select>
                                                    <input type="tel" min="1" max="20" defaultValue="1" maxLength="2" placeholder="Nv" ref={(r) => (this.nvComp = r)}/>&nbsp;
                                                    <button className="btnConfirma" onClick={(e) => this.AddCompetencia(e)}>&#10003;</button>
                                                </p>
                                            </form>
                                            {
                                            fichaAberta.atributos.fisico[1] !== undefined ?
                                            <>
                                            {
                                                Object.entries(fichaAberta.atributos.fisico[1]).map(competencia => (
                                                    <>
                                                    <p key={fichaAberta.id + competencia[0]}>
                                                        <b>[F] </b>{competencia[0]} <input name={"fisico@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => this.AlteraFicha(e)}/>
                                                    </p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                            }

                                            {
                                            fichaAberta.atributos.coordenacao[1] !== undefined ?
                                            <>
                                            {
                                                Object.entries(fichaAberta.atributos.coordenacao[1]).map(competencia => (
                                                    <>
                                                    <p key={fichaAberta.id + competencia[0]}><b>[C] </b>{competencia[0]} <input name={"coordenacao@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => this.AlteraFicha(e)}/></p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                            }
                                            {
                                            fichaAberta.atributos.inteligencia[1] !== undefined ?
                                            <>
                                            {
                                                Object.entries(fichaAberta.atributos.inteligencia[1]).map(competencia => (
                                                    <>
                                                    <p key={fichaAberta.id + competencia[0]}><b>[I] </b>{competencia[0]} <input name={"inteligencia@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => this.AlteraFicha(e)}/></p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                            }
                                            {
                                            fichaAberta.atributos.astucia[1] !== undefined ?
                                            <>
                                            {
                                                Object.entries(fichaAberta.atributos.astucia[1]).map(competencia => (
                                                    <>
                                                    <p key={fichaAberta.id + competencia[0]}><b>[A] </b>{competencia[0]} <input name={"astucia@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => this.AlteraFicha(e)}/></p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                            }
                                            {
                                            fichaAberta.atributos.vontade[1] !== undefined ?
                                            <>
                                            {
                                                Object.entries(fichaAberta.atributos.vontade[1]).map(competencia => (
                                                    <>
                                                    <p key={fichaAberta.id + competencia[0]}><b>[V] </b>{competencia[0]} <input name={"vontade@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => this.AlteraFicha(e)}/></p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                            }
                                            {
                                            fichaAberta.atributos.presenca[1] !== undefined ?
                                            <>
                                            {
                                                Object.entries(fichaAberta.atributos.presenca[1]).map(competencia => (
                                                    <>
                                                    <p key={fichaAberta.id + competencia[0]}><b>[P] </b>{competencia[0]} <input name={"presenca@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => this.AlteraFicha(e)}/></p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                        }
                                        </div>
                                    </div>

                                    <h4 onClick={(e) => this.AlternaSecao(e,"inventario")}>INVENTÁRIO</h4>
                                    <div name="inventario" className="bloco c1 fechado">
                                        <p><button type="button" className="btnRedondo btnConfirma" onClick={(e) => this.AbreFormInterno(e.target,"formItens")}>+</button></p>
                                        <div>
                                            <form id="formItens" className="formInterno fechado">
                                                <p>
                                                    <input placeholder="Nome item" ref={(r) => (this.opItens = r)}/>
                                                    &nbsp;
                                                    <button className="btnConfirma" onClick={(e) => this.AddItem(e)}>&#10003;</button>
                                                </p>
                                            </form>
                                            {
                                            fichaAberta.itens !== undefined && fichaAberta.itens.length > 0 ?
                                            <>
                                            {
                                                fichaAberta.itens.map(item => (
                                                    <>
                                                    <p key={fichaAberta.id + item.nome}>
                                                        {item.quantidade}x <b onClick={() => {this.MostraInfo(item.descricao)}}><u>{item.nome}</u></b> <button className="btnRemover" type="button" onClick={(e) => this.RemoveDado("itens",item.nome)}>&times;</button>
                                                    </p>
                                                    </>
                                                ))
                                            }
                                            </> : undefined
                                            }
                                        </div>
                                    </div>

                                    <h4 onClick={(e) => this.AlternaSecao(e,"ferimentos")}>FERIMENTOS</h4>
                                    <div name="ferimentos" className="bloco c2 fechado">
                                        <div className="b1">
                                            <p><b>Nível 1</b>:&nbsp; <input name="ferimentos@0" type="tel" className="selecionaTudo" defaultValue={fichaAberta.ferimentos[0]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>

                                            <p><b>Nível 3</b>:&nbsp; <input name="ferimentos@2" type="tel" className="selecionaTudo" defaultValue={fichaAberta.ferimentos[2]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>

                                            <p><b>Nível 5</b>:&nbsp; <input name="ferimentos@4" type="tel" className="selecionaTudo" defaultValue={fichaAberta.ferimentos[4]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                        </div>
                                        <div className="b2">
                                            <p><b>Nível 2</b>:&nbsp; <input name="ferimentos@1" type="tel" className="selecionaTudo" defaultValue={fichaAberta.ferimentos[1]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>

                                            <p><b>Nível 4</b>:&nbsp; <input name="ferimentos@3" type="tel" className="selecionaTudo" defaultValue={fichaAberta.ferimentos[3]} maxLength="2" onChange={(e) => this.AlteraFicha(e)}/></p>
                                        </div>
                                    </div>
                                    
                                    <h4 onClick={(e) => this.AlternaSecao(e,"rolagem")}>ROLAGEM</h4>
                                    <div name="rolagem" className="bloco c1 fechado">
                                        <div className="parametros">
                                        
                                            {/* <p>
                                                <label htmlFor="tipoCompetencia">Rola competência</label><input id="tipoCompetencia" type="checkbox" onChange={(e) => this.ParametrosRolagem(e)}/>
                                            </p> */}
                                            <p>
                                                <select id="rolAtributo" onChange={this.ParametrosRolagem}>
                                                    <option value="fisico">Físico</option>
                                                    <option value="coordenacao">Coordenação</option>
                                                    <option value="inteligencia">Inteligência</option>
                                                    <option value="astucia">Astúcia</option>
                                                    <option value="vontade">Vontade</option>
                                                    <option value="presenca">Presença</option>
                                                </select>
                                                &nbsp; {fichaAberta.atributos[state.rolagem.atributo][0]}
                                            </p>
                                            {
                                            state.rolagem.tipoCompetencia || true ? 
                                            <>
                                            <p>
                                                <select id="rolCompetencia" onChange={this.ParametrosRolagem}></select>&nbsp; {state.rolagem.competencia}
                                            </p>
                                            </> : undefined
                                            }
                                            <p>Modificador: <input id="rolBonus" type="tel" defaultValue="0" onChange={this.ParametrosRolagem}/></p>
                                            <p>Dificuldade: <input id="rolDificuldade" type="tel" defaultValue="50" onChange={this.ParametrosRolagem}/></p>
                                        </div>
                                        <div>
                                            <p>
                                                <button type="button" className="btnVazadoConfirma" onClick={this.ExecutaRolagem}>Rolar</button>
                                            </p>
                                            <p id="resultado"></p>
                                        </div>
                                    </div>

                                    
                                    <h4 style={fichaAberta.jogador.id === props.usuario.id ? undefined : {"display":"none"}} onClick={(e) => this.AlternaSecao(e,"excluir")}>EXCLUIR</h4>
                                    <div style={fichaAberta.jogador.id === props.usuario.id ? undefined : {"display":"none"}}  name="excluir" className="bloco c1 fechado">
                                        <p>Deseja mesmo excluir a ficha? Essa ação não poderá ser desfeita.</p>
                                        <p><button type="button" onClick={this.ExcluiFicha}>EXCLUIR</button></p>
                                    </div>
                                </div>
                                </>
                                : undefined
                                }
                                </>
                            }[state.opOver]
                        }
                        </div>
                        {
                            
                            <>
                            <div className={state.apareceSalvar ? "slotSalvar apareceSalva" : "slotSalvar escondeSalva"}>
                                <button id="salvaFicha" className={state.apareceSalvar ? "btn" : "btn escondeSalva"} onClick = {(e) => this.MandaSalvaFicha({...fichaAberta})}>SALVAR</button>
                            </div>
                            </>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Fichas;
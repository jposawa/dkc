import React, {Component} from 'react';

export default class Ficha extends Component{
    constructor(props){
        super(props);

        this.state = {};
    }

    RenderizaDados = (dados) =>
    {
        // console.log("entrou");
        if(dados === undefined || dados === null || dados === "")
        {
            dados = "competencias";
        }

        let opcoes = "";
        const {competencias, tracos, itens} = this.props.geral;
        
        console.log(dados);

        switch(dados)
        {
            case "competencias":    
                const {atributoEscolhido} = this.state;
                // console.log(atributoEscolhido);
                // console.log(competencias[atributoEscolhido] !== undefined && competencias[atributoEscolhido] !== null);
                if(competencias[atributoEscolhido] !== undefined && competencias[atributoEscolhido] !== null)
                {
                    competencias[atributoEscolhido].forEach(competencia => {
                        opcoes += "<option value=\""+competencia.nome+"\">"+competencia.nome+" ["+competencia.custo+"]</option>";
                    });
                    // console.log(this.opCompetencias);
                    // console.log(opcoes);
                    this.opCompetencias.innerHTML = opcoes;
                }
            break;

            case "tracos":
                if(tracos !== null && tracos !== undefined)
                {
                    tracos.forEach(traco => {
                        // console.log(traco);
                        opcoes += "<option value = \"" + traco.nome + "\">"+traco.nome + " ["+traco.custo+"]</option>";            
                    })
            
                    this.opTracos.innerHTML = opcoes;
                }
            break;

            default:
                console.log("Entrou em nenhum");
            break;
        }
    }

    render()
    {
        const {props, state} = this;
        const {geral, geralAnterior} = props;
        const {fichaAberta, atributoEscolhido, rolagem} = geral;

        return(
            <div id="slotPainelFicha" onClick = {props.CliqueOverlay}>
                <div id="painelFicha">
                    <button type="button" className="btn btnFechaOver" onClick={() => props.ChamaOp()}>&times;</button>
                    <div>
                    {
                        fichaAberta !== undefined ?
                        <>
                        <h3>{fichaAberta.nome}</h3>
                        <p>   
                            [{fichaAberta.raca}]
                        </p>
                        <p><b>Mesa: </b>{geralAnterior.listaMesas[fichaAberta.mesa].nome} ({fichaAberta.modulo})</p>
                        <h4>Pontos</h4>
                        <p>
                            <span className={fichaAberta.pontosTotais < fichaAberta.pontosGastos ? "txtAlerta" : undefined}>
                                {fichaAberta.pontosGastos}&nbsp;
                            </span>|&nbsp;{fichaAberta.pontosTotais}
                        </p>
                        <div id="corpo">
                            <h4 className="cabecalhoAtivo" onClick={(e) => props.AlternaSecao(e,"atributos")}>ATRIBUTOS</h4>
                            <div name="atributos" className="bloco c2">
                                <div className="b1">
                                    <p><b>Físico</b>:&nbsp; <input name="fisico" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.fisico[0]} maxLength="2" onChange={(e) => props.AlteraFicha(e)}/></p>
                                    <p><b>Inteligência</b>:&nbsp; <input name="inteligencia" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.inteligencia[0]} maxLength="2" onChange={(e) => props.AlteraFicha(e)}/></p>
                                    <p><b>Vontade</b>:&nbsp; <input name="vontade" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.vontade[0]} maxLength="2" onChange={(e) => props.AlteraFicha(e)}/></p>
                                </div>
                                <div className="b2">
                                    <p><b>Coordenação</b>:&nbsp; <input name="coordenacao" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.coordenacao[0]} maxLength="2" onChange={(e) => props.AlteraFicha(e)}/></p>
                                    <p><b>Astúcia</b>:&nbsp; <input name="astucia" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.astucia[0]} maxLength="2" onChange={(e) => props.AlteraFicha(e)}/></p>
                                    <p><b>Presença</b>:&nbsp; <input name="presenca" type="tel" className="selecionaTudo" defaultValue={fichaAberta.atributos.presenca[0]} maxLength="2" onChange={(e) => props.AlteraFicha(e)}/></p>
                                </div>
                            </div>

                            <h4 onClick={(e) => props.AlternaSecao(e,"tracos")}>TRAÇOS</h4>
                            <div name="tracos" className="bloco c1 fechado">
                                <p><button type="button" className="btnRedondo btnConfirma" onClick={(e) => props.AbreFormInterno(e.target,"formTracos")}>+</button></p>
                                <div>
                                    <form id="formTracos" className="formInterno fechado">
                                        <p>
                                            <select ref={(r) => (this.opTracos = r)}>
                                            </select>
                                            &nbsp;
                                            <button className="btnConfirma" onClick={(e) => props.AddTraco(e)}>&#10003;</button>
                                        </p>
                                    </form>
                                    {
                                    fichaAberta.tracos !== undefined && fichaAberta.tracos.length > 0 ?
                                    <>
                                    {
                                        fichaAberta.tracos.map(traco => (
                                            <>
                                            <p key={fichaAberta.id + traco.nome}>
                                                <b onClick={() => {props.MostraInfo(traco.descricao)}}><u>{traco.nome}</u></b> <button className="btnRemover" type="button" onClick={(e) => props.RemoveDado("tracos",traco.nome)}>&#9842;</button>
                                            </p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                    }
                                </div>
                            </div>

                            <h4 onClick={(e) => props.AlternaSecao(e,"competencias")}>COMPETÊNCIAS</h4>
                            <div name="competencias" className="bloco c1 fechado">
                                <p><button type="button" className="btnRedondo btnConfirma" onClick={(e) => props.AbreFormInterno(e.target,"formCompetencias")}>+</button></p>
                                <div>
                                    <form id="formCompetencias" className="formInterno fechado">
                                        <p>
                                            <select name="atributos" onChange={(e) => {props.setState({atributoEscolhido:e.target.value})}}>
                                                <option value="fisico">Físico</option>
                                                <option value="coordenacao">Coordenação</option>
                                                <option value="inteligencia">Inteligência</option>
                                                <option value="astucia">Astúcia</option>
                                                <option value="vontade">Vontade</option>
                                                <option value="presenca">Presença</option>
                                            </select>
                                            <select ref={(r) => (this.opCompetencias = r)}>
                                            </select>
                                            <input type="tel" min="1" max="20" defaultValue="1" maxLength="2" placeholder="Nv" ref={(r) => (this.nvComp = r)}/>&nbsp;
                                            <button className="btnConfirma" onClick={(e) => props.AddCompetencia(e)}>&#10003;</button>
                                        </p>
                                    </form>
                                    {
                                    fichaAberta.atributos.fisico[1] !== undefined ?
                                    <>
                                    {
                                        Object.entries(fichaAberta.atributos.fisico[1]).map(competencia => (
                                            <>
                                            <p key={fichaAberta.id + competencia[0]}>
                                                <b>[F] </b>{competencia[0]} <input name={"fisico@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => props.AlteraFicha(e)}/>
                                            </p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                    }

                                    {
                                    fichaAberta.atributos.coordenacao[1] !== undefined ?
                                    <>
                                    {
                                        Object.entries(fichaAberta.atributos.coordenacao[1]).map(competencia => (
                                            <>
                                            <p key={fichaAberta.id + competencia[0]}><b>[C] </b>{competencia[0]} <input name={"coordenacao@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => props.AlteraFicha(e)}/></p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                    }
                                    {
                                    fichaAberta.atributos.inteligencia[1] !== undefined ?
                                    <>
                                    {
                                        Object.entries(fichaAberta.atributos.inteligencia[1]).map(competencia => (
                                            <>
                                            <p key={fichaAberta.id + competencia[0]}><b>[I] </b>{competencia[0]} <input name={"inteligencia@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => props.AlteraFicha(e)}/></p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                    }
                                    {
                                    fichaAberta.atributos.astucia[1] !== undefined ?
                                    <>
                                    {
                                        Object.entries(fichaAberta.atributos.astucia[1]).map(competencia => (
                                            <>
                                            <p key={fichaAberta.id + competencia[0]}><b>[A] </b>{competencia[0]} <input name={"astucia@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => props.AlteraFicha(e)}/></p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                    }
                                    {
                                    fichaAberta.atributos.vontade[1] !== undefined ?
                                    <>
                                    {
                                        Object.entries(fichaAberta.atributos.vontade[1]).map(competencia => (
                                            <>
                                            <p key={fichaAberta.id + competencia[0]}><b>[V] </b>{competencia[0]} <input name={"vontade@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => props.AlteraFicha(e)}/></p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                    }
                                    {
                                    fichaAberta.atributos.presenca[1] !== undefined ?
                                    <>
                                    {
                                        Object.entries(fichaAberta.atributos.presenca[1]).map(competencia => (
                                            <>
                                            <p key={fichaAberta.id + competencia[0]}><b>[P] </b>{competencia[0]} <input name={"presenca@"+competencia[0]} type="tel" defaultValue={competencia[1]} onChange={(e) => props.AlteraFicha(e)}/></p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                }
                                </div>
                            </div>

                            <h4 onClick={(e) => props.AlternaSecao(e,"inventario")}>INVENTÁRIO</h4>
                            <div name="inventario" className="bloco c1 fechado">
                                <p><button type="button" className="btnRedondo btnConfirma" onClick={(e) => props.AbreFormInterno(e.target,"formItens")}>+</button></p>
                                <div>
                                    <form id="formItens" className="formInterno fechado">
                                        <p>
                                            <input placeholder="Nome item" ref={(r) => (this.opItens = r)}/>
                                            &nbsp;
                                            <button className="btnConfirma" onClick={(e) => props.AddItem(e)}>&#10003;</button>
                                        </p>
                                    </form>
                                    {
                                    fichaAberta.itens !== undefined && fichaAberta.itens.length > 0 ?
                                    <>
                                    {
                                        fichaAberta.itens.map(item => (
                                            <>
                                            <p key={fichaAberta.id + item.nome}>
                                                {item.quantidade}x <b onClick={() => {props.MostraInfo(item.descricao)}}><u>{item.nome}</u></b> <button className="btnRemover" type="button" onClick={(e) => props.RemoveDado("itens",item.nome)}>&#128465;</button>
                                            </p>
                                            </>
                                        ))
                                    }
                                    </> : undefined
                                    }
                                </div>
                            </div>

                            <h4 onClick={(e) => props.AlternaSecao(e,"rolagem")}>ROLAGEM</h4>
                            <div name="rolagem" className="bloco c1 fechado">
                                <div>
                                <br/>
                                    {/* <p>
                                        <label htmlFor="tipoCompetencia">Rola competência</label><input id="tipoCompetencia" type="checkbox" onChange={(e) => props.ParametrosRolagem(e)}/>
                                    </p> */}
                                    <p>
                                        <select id="rolAtributo" onChange={props.ParametrosRolagem}>
                                            <option value="fisico">Físico</option>
                                            <option value="coordenacao">Coordenação</option>
                                            <option value="inteligencia">Inteligência</option>
                                            <option value="astucia">Astúcia</option>
                                            <option value="vontade">Vontade</option>
                                            <option value="presenca">Presença</option>
                                        </select>
                                        &nbsp; {fichaAberta.atributos[rolagem.atributo][0]}
                                    </p>
                                    {
                                    rolagem.tipoCompetencia || true ? 
                                    <>
                                    <p>
                                        <select id="rolCompetencia" onChange={props.ParametrosRolagem}></select>&nbsp; {rolagem.competencia}
                                    </p>
                                    </> : undefined
                                    }
                                    <p>Modificador: <input id="rolBonus" type="tel" defaultValue="0" onChange={props.ParametrosRolagem}/></p>
                                    <p>Dificuldade: <input id="rolDificuldade" type="tel" defaultValue="50" onChange={props.ParametrosRolagem}/></p>
                                    <p>
                                        <button type="button" className="btnVazadoConfirma" onClick={props.ExecutaRolagem}>Rolar</button>
                                    </p>
                                    <p id="resultado"></p>
                                </div>
                            </div>

                            <h4 onClick={(e) => props.AlternaSecao(e,"excluir")}>EXCLUIR</h4>
                            <div name="excluir" className="bloco c1 fechado">
                                <p>Deseja mesmo excluir a ficha? Essa ação não poderá ser desfeita.</p>
                                <p><button type="button" onClick={props.ExcluiFicha}>EXCLUIR</button></p>
                            </div>
                        </div>
                        </>
                        : undefined  
                    }
                    </div>
                    {
                        
                        <>
                        <div className={state.apareceSalvar ? "btn apareceSalva" : "btn escondeSalva"}>
                            <button id="salvaFicha" className={state.apareceSalvar ? "btn apareceSalva" : "btn escondeSalva"} onClick = {(e) => props.SalvaFicha({...fichaAberta})}>SALVAR</button>
                        </div>
                        </>
                    }
                </div>
            </div>
        )
    }
}
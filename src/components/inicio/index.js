import React, {Component} from 'react';

class Inicio extends Component{
    constructor(props){
        super(props);
        
        this.state={};
    }

    componentDidMount()
    {

    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = prevProps.geral;
        const {geral} = props;
    }

    render()
    {
        const {state, props} = this;

        return(
            <div>
                <img className="imagemInicial" src={require("../../img/logo1.png")}/>
                <h1>Draenak Companion</h1>
                <div className="caixaTexto alinhadoEsquerda">
                    <p>Olá, é muito bom te ver por aqui.</p>
                    <p>Eu imagino que você não esteja entendendo o que tem em mãos...</p>
                    <p>Imagine como um Grimório em que você pode salvar as suas várias facetas, ou como deve chamar, as suas "fichas".</p>
                    <br/>
                    <p>Minhas funcionalidades ainda estão em construção pelo mago que me criou, portanto eu ainda posso apresentar algumas instabilidades.</p>
                    <p>Mesmo assim, se você está aqui, sinta-se à vontade para explorar o menu inferior e ver o que pode encontrar.</p>
                </div>
            </div>
        );
    }
}

export default Inicio;
import React, {Component} from 'react';
import './styles.css';

class Mesas extends Component{
    constructor(props){
        super(props);
        
        this.state={
            opOver:undefined,
            painelOver:false,
            // modulo:"PRINCIPAL",
            atributoEscolhido:"fisico",
            apareceSalvar:false,
            mostraModulos:true,
            rolagem:{
                tipoCompetencia:false,
                atributo:"fisico",
                competencia:0,
                bonus:0,
                dificuldade:50,
            },
        };
    }

    componentDidMount()
    {
        const {props} = this;
        const {geral} = props;

        props.PuxaMesas();
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = {...prevProps.geral};
        const {geral} = props;

        if(prevState !== null && prevState !== undefined)
        {
            if(prevState.opOver !== state.opOver && state.opOver === "criaMesa")
            {
                this.CarregaModulos();
            }
        }
    }

    ChamaOp = (operacao) =>
    {
        let painelOver = false;
        if(operacao !== undefined && operacao !== null)
        {
            operacao = operacao.trim();
            if(operacao !== "")
            {
                painelOver = true;
            }
        }

        this.setState({opOver:operacao, painelOver:painelOver});
    }

    CarregaModulos = () =>
    {
        const {state, props} = this;
        const {geral} = props;
        const {listaModulos} = geral;

        let opcoes = "";

        listaModulos.forEach(modulo => {
            opcoes += "<option value=\""+modulo+"\">";
            opcoes += modulo;
            opcoes += "</option>";
        });

        this.modulo.innerHTML = opcoes;
    }

    CriaMesa = (evento, nomeMesa, moduloEscolhido) =>
    {
        evento.preventDefault();
        const {state,props} = this;
        const {usuario} = props;
        const mesa = {};
        nomeMesa = nomeMesa.trim();

        if(nomeMesa === undefined || nomeMesa === "")
        {
            alert("A mesa precisa ter um nome");
            this.nomeMesa.focus();
            return false;
        }

        mesa.nome = nomeMesa;
        mesa.aberta = true;
        mesa.membros = {};
        mesa.membros[usuario.id] = {
            id:usuario.id,
            email:usuario.email,
            nvAcesso:5,
        };
        mesa.modulo = moduloEscolhido;

        props.SalvaMesa(mesa);

        this.DetalhaMesa(mesa);
    }

    EntraMesa = (evento, codigoMesa) =>
    {
        evento.preventDefault();
        const {state, props} = this;
        const {geral,usuario} = props;
        const {totalMesas} = geral;
        const infoMesas = Object.values(totalMesas);
        let contador = 0;
        let encontrou = false;
        let mesa;
        codigoMesa = codigoMesa.toLowerCase();

        while(contador < infoMesas.length && !encontrou)
        {
            mesa = infoMesas[contador++];
            console.log(mesa);
            if(mesa.codigo === codigoMesa && mesa.aberta)
            {
                encontrou = true;
            }
        }

        if(!encontrou)
        {
            alert("O código informado não corresponde a nenhuma mesa, ou ela está fechada");
        }
        else
        {
            // const membrosMesa = Object.keys(mesa.membros);

            if(mesa.membros[usuario.id] !== null && mesa.membros[usuario.id] !== undefined)
            {
                alert("Você já está nessa mesa");
                this.codigoMesa.focus();
            }
            else
            {
                mesa.membros[usuario.id] = {
                    id:usuario.id,
                    email:usuario.email,
                    nvAcesso:1,
                };
                props.SalvaMesa(mesa);
                this.DetalhaMesa(mesa);
            }
        }
        console.log(codigoMesa);
    }

    DetalhaMesa = mesa =>
    {
        if(mesa !== undefined && mesa !== null)
        {
            this.ChamaOp("detalhaMesa");
            this.props.AlteraModulo(mesa.modulo);
            this.setState({
                mesaAberta:{...mesa},
            });
        }
    }

    ExcluiMesa = (e) =>
    {
        const {mesaAberta} = this.state;

        const confirma = window.confirm("Você tem certeza? A ação é irreversível");

        if(confirma)
        {
            mesaAberta.exclui = true;
            this.setState({
                opOver:undefined,
                painelOver:false,
            });
            this.props.SalvaMesa(mesaAberta);
        }
    }

    AlternaSecao = (evento, nomeSecao) =>
    {
        const campo = evento.target;
        // console.log(evento.target);
        if(nomeSecao !== null && nomeSecao !== undefined)
        {
            nomeSecao = nomeSecao.trim();
            if(nomeSecao !== "")
            {
                let listaSecao = document.getElementsByName(nomeSecao);

                if(listaSecao.length > 0)
                {
                    let secao = listaSecao[0];
                    if(!secao.classList.toggle("fechado"))
                    {
                        if(nomeSecao === "competencias")
                        {
                            this.RenderizaDados();
                        }
                    }
                    campo.classList.toggle("cabecalhoAtivo");
                }
            }
        }
    }

    AtualizaNvAcesso = (evento, idMembro) =>
    {
        const {state,props} = this;
        const {geral} = props;
        const {mesaAberta} = state;
        const campo = evento.target;
        
        let confirma;
        let novoNv;

        // console.log(campo);

        // console.log(campo.checked);
        // console.log(idMembro);
        // console.log(mesaAberta.membros[idMembro]);
        if(campo.checked)
        {
            confirma = window.confirm("Confirma atribuição de cargo de admin para " + mesaAberta.membros[idMembro].email + "?");
            novoNv = 2;
        }
        else
        {
            confirma = window.confirm("Confirma revogação de cargo de admin para " + mesaAberta.membros[idMembro].email + "?");
            novoNv = 1;
        }

        if(confirma)
        {
            mesaAberta.membros[idMembro].nvAcesso = novoNv;
            props.SalvaMesa(mesaAberta);
        }
        else
        {
            campo.checked = !campo.checked;
        }
    }

    render()
    {
        const {state, props} = this;
        const {geral, usuario} = props;
        const {listaMesas, listaModulos} = geral;
        const {mesaAberta} = state;
        // const {fichaAberta, atributoEscolhido} = state;

        return(
            <div id="bloco">
                <div>
                    <h1>Mesas</h1>
                    <button type="button" onClick={() => {this.ChamaOp("criaMesa")}} className="btnConfirma">+</button>
                    {(listaMesas !== undefined && listaMesas !== null) ?
                    <ul id="listaMesas" className="listaCards">
                        {Object.values(listaMesas).map(mesa => {
                            if(mesa.id !== "" && mesa.id !== undefined)
                            {
                                {/* console.log(mesa); */}
                                return(
                                    <li key={mesa.id} onClick = {() => {this.DetalhaMesa({...mesa})}}>
                                        <div>
                                            <p><b>{mesa.nome}</b></p>
                                            <p>Total membros: {Object.values(mesa.membros).length}</p>
                                        </div>
                                        <div className="resumo c1">
                                            <div>
                                                Código: <b><span className="txtAzul">{mesa.codigo.toUpperCase()}</span></b>
                                            </div>
                                        </div>
                                    </li>
                                );
                            }
                        })}
                    </ul>
                    : undefined
                    }
                </div>
                <div id="slotPainelOver" style={(state.opOver === null || state.opOver === undefined || state.opOver === "") ? {opacity:0,visibility:"hidden"} : {visibility:"visible",opacity:1}} onClick = {this.CliqueOverlay}>
                    <div id="painelOver" className="painelRecuado">
                        <button type="button" className="btn btnFechaOver" onClick={() => this.ChamaOp()}>&times;</button>
                        <div>
                        {
                            {
                                'criaMesa':
                                <div id="corpo">
                                    <h3>Nova mesa</h3>
                                    <h4 onClick={(e) => this.AlternaSecao(e,"criar")}>CRIAR MESA</h4>
                                    <div name="criar" className="bloco c1 fechado">
                                        <p>
                                            Nome:&nbsp;
                                            <input type="text" placeholder="Nome da mesa" ref={(r) => {this.nomeMesa = r}} autoFocus={true}/>
                                        </p>

                                        <p>
                                            Módulo:&nbsp;
                                            <select ref={r => this.modulo = r}>
                                                {
                                                    listaModulos.forEach(modulo => (
                                                        <option value={modulo}>{modulo}</option>
                                                    ))
                                                }
                                            </select>
                                        </p>
                                        <p><button className="btn btnConfirma" onClick = {e => this.CriaMesa(e, this.nomeMesa.value, this.modulo.value)}>Criar</button></p>
                                    </div>
                                    
                                    <h4 onClick={(e) => this.AlternaSecao(e,"entrar")}>ENTRAR MESA</h4>
                                    <div name="entrar" className="bloco c1 fechado">
                                        <p>
                                            Código mesa:&nbsp;
                                            <input type="text" placeholder="Código da mesa" ref={(r) => {this.codigoMesa = r}} onKeyUp = {props.CampoMaiusculo} maxLength="14"/>
                                        </p>

                                        <p><button className="btn btnConfirma" onClick = {e => this.EntraMesa(e, this.codigoMesa.value)}>Entrar</button></p>
                                    </div>
                                </div>,

                                'detalhaMesa':
                                <>
                                {mesaAberta !== undefined ?
                                <>
                                <h3>{mesaAberta.nome}</h3>
                                <p>{mesaAberta.modulo}</p>
                                <p>Código: <b><span className="txtAzul">{mesaAberta.codigo.toUpperCase()}</span></b></p>
                                
                                <div id="corpo">
                                    <h4 onClick={(e) => this.AlternaSecao(e,"membros")}>MEMBROS</h4>
                                    <div name="membros" className="bloco c1 fechado">
                                        <ul>
                                        {mesaAberta === null || mesaAberta === undefined ? undefined : Object.values(mesaAberta.membros).map(membro => {
                                            return(
                                            <li key={membro.email} className={membro.nvAcesso === 5 ? "elemListaDuplo txtDono" : membro.nvAcesso > 1 ? "elemListaDuplo txtAdmin" : "elemListaDuplo"}>
                                                {membro.email.split('@')[0]}
                                                {membro.nvAcesso < 5 && mesaAberta.membros[usuario.id].nvAcesso > 1 ?
                                                <>
                                                <label for={membro.email}><p>Admin</p><span>&nbsp;</span></label>
                                                <input 
                                                    id={membro.email} 
                                                    onChange={(e) => this.AtualizaNvAcesso(e, membro.id)} 
                                                    type="checkbox"
                                                    defaultChecked={(membro.nvAcesso > 1)}
                                                />
                                                </> : membro.nvAcesso === 5 ?
                                                <span>Dono</span> : membro.nvAcesso > 1 ?
                                                <span>Admin</span> : undefined
                                                }
                                            </li>
                                        )})}
                                        </ul>
                                    </div>

                                    <h4 onClick={(e) => this.AlternaSecao(e,"excluir")}>EXCLUIR</h4>
                                    <div name="excluir" className="bloco c1 fechado">
                                        <p>Deseja mesmo excluir a mesa? Essa ação não poderá ser desfeita.</p>
                                        <p><button type="button" onClick={this.ExcluiMesa}>EXCLUIR</button></p>
                                    </div>
                                </div>
                                </>
                                : undefined
                                }
                                </>
                            }[state.opOver]
                        }
                        </div>
                        {
                            
                            <>
                            <div className={state.apareceSalvar ? "btn apareceSalva" : "btn escondeSalva"}>
                                <button id="salvaMesa" className={state.apareceSalvar ? "btn apareceSalva" : "btn escondeSalva"} onClick = {(e) => props.SalvaMesa({...mesaAberta})}>SALVAR</button>
                            </div>
                            </>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Mesas;
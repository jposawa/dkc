import React, {Component} from 'react';

class Menu extends Component{
    constructor(props){
        super(props);
        
        this.state={};
        this.dadosTela = {
            alturaTela:undefined,
            alturaMenu:undefined,
        };
    }

    componentDidMount()
    {
        this.dadosTela.alturaTela = window.innerHeight;
        this.dadosTela.alturaMenu = this.dadosTela.alturaTela;
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = prevProps.geral;
        const {geral} = props;
        if(prevGeral.menuAberto !== geral.menuAberto && !geral.menuAberto)
        {
            this.FechaPainel();
        }
    }

    AlternaMenu = (evento)=>{        
        //console.log(this.state.menuAberto);
        // console.log("entrouA");
        // console.log(evento.target);
        const {props} = this;
        const {geral} = props;
        const painelMenu = document.getElementById("slotMenu");
        const iconeMenu = document.getElementById("iconeMenu");

        // console.log(geral.menuAberto);
        // let proxEstado = "";
        let grauGiro = iconeMenu.style.transform;
        // console.log(grauGiro);
        if(grauGiro !== undefined && grauGiro !== null && grauGiro !== "")
        {
            grauGiro = grauGiro.split("(");
            grauGiro = grauGiro[1].split("deg");
            // console.log(grauGiro[0]);
            grauGiro = parseInt(grauGiro[0]) + 180;
        }
        else
        {
            grauGiro = 180;
        }
        // console.log(grauGiro);
        if(geral.menuAberto)
        {
            //console.log(painelMenu.style.width);
            /* painelMenu.style.right = "calc(var(--larguraPainelDireita) - var(--larguraPainelMenu))";
            // proxEstado = "fechado";
            iconeMenu.style.transform = "rotate(0)"; */
            this.FechaPainel();
        }
        else
        {
            // let alturaMenu = getComputedStyle(painelMenu).getPropertyValue("--alturaMenu");
            let alturaMenu = painelMenu.offsetHeight;
            this.dadosTela.alturaMenu = this.dadosTela.alturaTela - alturaMenu;

            painelMenu.style.top = this.dadosTela.alturaMenu + "px";
            // painelMenu.style.top = "calc(100% - " + alturaMenu + ")";
            // proxEstado = "aberto";
            iconeMenu.style.transform = "rotateX("+grauGiro+"deg) rotateZ(90deg)";
        }

        props.AlternaMenu(!geral.menuAberto);
        //iconeMenu.style.transform = "rotate("+grauGiro+"deg)";
        // this.setState({menuAberto:!this.state.menuAberto});
    }

    FechaPainel = () =>
    {
        const painelMenu = document.getElementById("slotMenu");
        const iconeMenu = document.getElementById("iconeMenu");

        painelMenu.style.top = this.dadosTela.alturaTela + "px";
        // painelMenu.style.top = "100%";
        // proxEstado = "fechado";
        iconeMenu.style.transform = "rotateX(0) rotateZ(90deg)";
    }

    AlteraPagina = (evento) =>
    {
        const paginasValidas = ["FICHAS","MESAS","PERFIL", "SAIR"];
        // console.log(evento.target);
        let paginaAlvo = evento.target.innerHTML;
        const clicouFora = paginaAlvo.includes("<li>");
        // console.log(clicouFora);
        
        if(!clicouFora)
        {
            
            paginaAlvo = paginaAlvo.trim().toUpperCase();
            // console.log("entrou");
            if(paginasValidas.includes(paginaAlvo))
            {
                this.props.AlteraPagina(paginaAlvo);
            }
            else
            {
                this.props.AlteraPagina("INICIO");
            }
        }
        // console.log(paginaAlvo);
    }

    render()
    {
        const {state, props} = this;

        return(
            <div id="painelMenu">
                <button type="button" id="btnIconeMenu" onClick={this.AlternaMenu}><p id="iconeMenu">{"\u2039"}</p></button>
                <ul id="menu" onClick = {this.AlteraPagina}>
                    <img src={require("../../img/logo1.png")}/>
                    <li>INÍCIO</li>
                    <li>FICHAS</li>
                    <li>MESAS</li>
                    <li>PERFIL</li>
                    <li onClick={() => {props.Logout()}}>SAIR</li>
                </ul>
            </div>
        );
    }
}

export default Menu;
import React, {Component} from 'react';
import Routes from './routes.js'
import "./styles.css";

const App = () =>(
  <div className="App">
    <Routes/>
  </div>  
);

export default App;

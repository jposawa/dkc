import React, {Component} from 'react';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import "./styles.css";

import Menu from '../../components/menu';
import Inicio from '../../components/inicio';
import Perfil from '../../components/perfil';
import Fichas from '../../components/fichas';
import Mesas from '../../components/mesas';
import Construindo from "../../components/construindo";

const uuid = {
    v1: require('uuid/v1'),
    v4: require('uuid/v4'),
}

class Principal extends Component{
    constructor(props){
        super(props);

        this.auth = firebase.auth();
        this.db = firebase.database();
        
        this.state={
            menuAberto:false,
            pagina:"INICIO",
        };
    }

    componentDidMount()
    {
        this.ListaModulos();
        this.AlteraModulo();
        this.PuxaDados("PRINCIPAL", "competencias");
    }

    componentDidUpdate(prevProps, prevState)
    {
        const {props, state} = this;
        const prevGeral = {...prevProps.geral};
        const {geral} = props;

        // if(prevState.pagina !== state.pagina)
        // {
        /* switch(state.pagina)
        {
            case "FICHAS":
                this.PuxaFichas();
            break;
        } */
        // }
    }

    /*--- FICHAS ---*/
    SalvaFicha = (ficha) =>
    {
        // const {recemCriada} = ficha;
        // ficha.recemCriada = false;
        const {usuario} = this.props.geral;
        const idFicha = uuid.v4();
        let caminho = "/fichas/";
        let updates = {};

        if(ficha !== null && ficha !== undefined)
        {
            if(ficha.id === null || ficha.id === undefined)
            {
                ficha.id = idFicha;
                /* ficha.jogador = {
                    email:usuario.email,
                    id:usuario.id,
                }; */
            }
            if(ficha.jogador === null || ficha.jogador === undefined)
            {
                ficha.jogador = {
                    email:usuario.email,
                    id:usuario.id,
                }
            }

            if(ficha.ferimentos === null || ficha.ferimentos === undefined)
            {
                ficha.ferimentos = [0,0,0,0,0];
            }
            let codigo = ficha.id.substr(0,7).toUpperCase();
            ficha.codigo = codigo;
            caminho += ficha.jogador.id + "/" + ficha.mesa + "/" + ficha.id;
            // console.log(ficha);
            // console.log(ficha.exclui);
            if(ficha.exclui)
            {
                updates[caminho] = null;
            }
            else
            {
                updates[caminho] = {...ficha};
            }

            try
            {
                this.db.ref().update(updates).then(() => {
                    console.log("Ficha salva");
                });
            }
            catch(erro)
            {
                console.log(erro);
            }
        }
        // console.log(ficha);
    }

    PuxaFichas = () =>
    {
        // console.log("xablau");
        const {usuario} = this.props.geral;
        let caminhoFichas = "/fichas/";
        if(usuario.nvAcesso < 7)
        {
            caminhoFichas += usuario.id;
        }
        const referenciaFichas = this.db.ref(caminhoFichas);

        referenciaFichas.on('value', snapshot => {
            const resultado = snapshot.val();

            // console.log(resultado);
            this.OrganizaFichas(resultado);
        });
    }

    OrganizaFichas = (bruto) =>
    {
        const {state,props} = this;
        let listaFichas = [];
        let listaObjFichas = [];
        
        if(bruto !== null && bruto !== undefined)
        {
            const fichaMesa = Object.values(bruto);    
            // console.log(fichaMesa);
            fichaMesa.forEach(mesa => {
                // console.log(Object.values(mesa));
                listaObjFichas = listaObjFichas.concat([...Object.values(mesa)]);
                
            });
            // console.log(listaObjFichas);
            // console.log(Object.values(bruto));
            // console.log(listaObjFichas);
            if(props.geral.usuario.nvAcesso >=7)
            {
                listaObjFichas.forEach(objeto => {
                    // console.log(objeto);
                    listaFichas = listaFichas.concat([...Object.values(objeto)]);
                });
                // console.log(listaFichas);
            }
            else
            {
                listaFichas = listaObjFichas;
            }
            // console.log(listaFichas);
        }

        this.setState({listaFichas:listaFichas})

        /* listaFichas.forEach(ficha => {
            // console.log(ficha.atributos);
        }); */
        // console.log(listaFichas);
    }

    ListaModulos = () =>
    {
        const referencia = this.db.ref("/modulos/");

        referencia.on('value', snapshot => {
            const resultado = snapshot.val();
            const listaModulos = Object.keys(resultado);
            // console.log(listaModulos);
            this.setState({listaModulos:listaModulos});
        });
    }

    AlteraModulo = (novoModulo) =>
    {
        // const {modulo} = this.state;
        const {state} = this;
        // console.log(state);
        // console.log(novoModulo);
        const {modulo} = state;

        // console.log(modulo);
        if(novoModulo === undefined || novoModulo === null || novoModulo === "")
        {
            novoModulo = "PRINCIPAL";
        }

        if(modulo !== novoModulo)
        {
            // console.log("xablau");
            this.setState({modulo:novoModulo});
        }
    }

    PuxaCompetencias = (modulo) =>
    {
        if(modulo === null || modulo === undefined)
        {
            modulo = "PRINCIPAL";
        }
        else
        {
            modulo = modulo.toUpperCase();
        }

        const referenciaCompetencias = this.db.ref("/modulos/"+modulo+"/competencias/");

        referenciaCompetencias.on('value', snapshot => {
            const resultado = snapshot.val();

            // console.log(resultado);
            // return resultado;
            this.setState({modulo:modulo,competencias:resultado});
        });
    }

    PuxaDados = (modulo, tipoDado) =>
    {
        // console.log(modulo);
        if(modulo === null || modulo === undefined || modulo === "")
        {
            modulo = "PRINCIPAL";
        }
        else
        {
            modulo = modulo.toUpperCase();
        }

        if(tipoDado === null || tipoDado === undefined || tipoDado === "")
        {
            tipoDado = "competencias";
        }
        else
        {
            tipoDado = tipoDado.toLowerCase();
        }

        const referencia = this.db.ref("/modulos/"+modulo+"/"+tipoDado+"/");

        referencia.on('value', snapshot => {
            const resultado = snapshot.val();
            const stateAtual = this.state;

            // console.log(resultado);
            stateAtual[tipoDado] = resultado;
            // stateAtual["modulo"] = modulo;
            this.setState(stateAtual);
        });
    }
    /*--- FICHAS ---*/

    /*--- RAÇAS ---*/
    PuxaRacas = () =>
    {
        const {state} = this;
        const {modulo} = state;

        let caminho = "modulos/"+modulo+"/racas/";
        // console.log(modulo);
        const referencia = this.db.ref(caminho);
        
        referencia.on('value', snapshot =>{
            const resultado = snapshot.val();

            // console.log(resultado);
            this.setState({listaRacas:resultado});
        })
    }

    OrganizaRacas = (racas) =>
    {}
    /*--- RAÇAS ---*/

    /*--- MESAS ---*/
    SalvaMesa = (mesa) =>
    {
        const {usuario} = this.props.geral;
        const idMesa = uuid.v1();
        let caminho = "/mesas/";
        let updates = {};

        if(mesa !== null && mesa !== undefined)
        {
            if(mesa.id === null || mesa.id === undefined)
            {
                mesa.id = idMesa;
                /* mesa.jogador = {
                    email:usuario.email,
                    id:usuario.id,
                }; */
            }
            let codigo = mesa.id.split('-');

            mesa.codigo = codigo[3];

            let mensagem = "Mesa salva";
            caminho += mesa.id;
            // console.log(mesa);
            // console.log(mesa.exclui);
            if(mesa.exclui)
            {
                updates[caminho] = null;
                mensagem = "Mesa excluída";
            }
            else
            {
                updates[caminho] = {...mesa};
            }

            try
            {
                this.db.ref().update(updates).then(() => {
                    console.log(mensagem);
                });
            }
            catch(erro)
            {
                console.log(erro);
            }
        }
    }

    PuxaMesas = () =>
    {
        // console.log("xablau");
        // const {usuario} = this.props.geral;
        let caminho = "/mesas/";
        /* if(usuario.nvAcesso < 9)
        {

            caminho += usuario.id;
        } */
        const referencia = this.db.ref(caminho);

        referencia.on('value', snapshot => {
            const resultado = snapshot.val();

            // console.log(resultado);
            this.OrganizaMesas(resultado);
        });
    }

    OrganizaMesas = totalMesas =>
    {
        const {usuario} = this.props.geral;
        let listaMesas = {};

        if(totalMesas !== null && totalMesas !== undefined)
        {
            // console.log(totalMesas);
            const todasMesas = {...totalMesas};
            if(usuario.nvAcesso < 9)
            {
                const mesasFiltradas = Object.values(totalMesas).filter(mesa => (Object.keys(mesa.membros).includes(usuario.id)));

                mesasFiltradas.forEach(mesa => {
                    listaMesas[mesa.id] = mesa;
                })
            }
            else
            {
                listaMesas = {...totalMesas};
            }

            listaMesas["SM"] = {
                nome:"Sem mesa"
            };

            this.setState({
                totalMesas:totalMesas,
                listaMesas:listaMesas
            });
        }
    }
    /*--- MESAS ---*/

    AlternaMenu = (menuAberto) =>
    {
        this.setState({menuAberto:menuAberto});
    }

    AlteraPagina = (nomePagina) =>
    {
        // console.log(nomePagina);
        nomePagina = nomePagina.trim();
        this.setState({
            pagina:nomePagina,
            // menuAberto:false
        });
    }

    CliqueTela = (evento) =>
    {
        // console.log("clicou tela");
        // console.log(evento.target);
        this.setState({
            menuAberto:false
        });
    }

    CampoMaiusculo = (evento) =>
    {
        const campo = evento.target;

        if(campo.value !== undefined && campo.value !== null)
        {
            campo.value = campo.value.toUpperCase();
        }
    }

    render()
    {
        const {state, props} = this;

        return(
            <>
                <div id="slotTela" onClick={this.CliqueTela}>
                    {
                        {
                            'INICIO': <Inicio/>,
                            'FICHAS': <Fichas
                                usuario = {props.geral.usuario}
                                geral = {state}
                                SalvaFicha = {this.SalvaFicha}
                                PuxaFichas = {this.PuxaFichas}
                                PuxaDados = {this.PuxaDados}
                                AlteraModulo = {this.AlteraModulo}
                                listaFichas = {state.listaFichas}
                                PuxaMesas = {this.PuxaMesas}
                                PuxaRacas = {this.PuxaRacas}
                            />,
                            'MESAS': <Mesas
                                usuario = {props.geral.usuario}
                                geral = {state}
                                SalvaMesa = {this.SalvaMesa}
                                PuxaMesas = {this.PuxaMesas}
                                AlteraModulo = {this.AlteraModulo}
                                CampoMaiusculo = {this.CampoMaiusculo}
                            />,
                            'PERFIL': <Construindo/>,
                            /* 'PERFIL': <Perfil
                                usuario = {props.geral.usuario}
                                AtualizaDados = {props.AtualizaDados}
                                AtualizaEmail = {props.AtualizaEmail}
                                AtualizaSenha = {props.AtualizaSenha}
                            />, */
                            
                        }[state.pagina]
                    }
                </div>
                <div id="slotMenu">
                    <Menu
                        geral = {this.state}
                        Logout = {props.Logout}
                        AlternaMenu = {this.AlternaMenu}
                        AlteraPagina = {this.AlteraPagina}
                    />
                </div>
            </>
        );
    }
}

export default Principal;
import React, { Component } from 'react';

import "./styles.css";

// import {usuario} from '../../infos.json';

// import Firebase from '../../components/Firebase';

import Cadastro from "./cadastro";

class Login extends Component{
    constructor(props){
        super(props);
        this.state ={
            usuario:{
                id:null,
                nvAcesso:null
            },
            cadastrar:false,
        }
        //this.ValidaForm = this.ValidaForm.bind(this);
    }


    AlternaCadastro = cadastrar =>
    {
        if(cadastrar === null || cadastrar === undefined)
        {
            cadastrar = false;
        }

        this.setState({cadastrar:cadastrar});
    }

    ValidaForm = (event) =>
    {
        this.props.ChamaLoad(true);
        event.preventDefault();
        let email = this.email.value;
        let senha = this.senha.value;

        if(email === null || email === "")
        {
            alert("O campo de e-mail precisa ser preenchido");
            this.email.focus();
            this.props.ChamaLoad();
        }
        else if(email.split("@")[1] === "" || email.split("@").length < 2)
        {
            alert("O campo de e-mail está inválido");
            this.email.focus();
            this.props.ChamaLoad();
        }
        else if(senha === null || senha === "")
        {
            alert("O campo de senha precisa ser preenchido");
            this.senha.focus();
            this.props.ChamaLoad();
        }
        else
        {
            this.props.LoginEmailSenha(email, senha);
            //this.BuscaUsuario(email, senha);
        }
    }

    ChamaRecuperaSenha = () =>{
        const email = this.email.value;

        if(email === null || email === undefined || email === "" || email.split("@")[1] === "" || email.split("@").length < 2)
        {
            alert("E-mail inválido");
            this.email.focus();
        }
        else
        {
            this.props.RecuperaSenha(email);
        }
    }

    ChamaAlternaCampoSenha = (botao) =>{
        botao = botao.target;
        let campo = this.senha;

        this.props.AlternaCampoSenha(botao, campo);
    }

    render(){
        const {state, props} = this;

        return(
            <div id="blocoLogin">
                {/* <img src={require('../../img/logoInicial.svg')}/> */}
                <form id="formLogin">  
                { state.cadastrar ?
                    <Cadastro
                        CadastroUsuario = {props.CadastroUsuario}
                        AlternaCadastro = {this.AlternaCadastro}
                        ChamaLoad = {props.ChamaLoad}
                        AlternaCampoSenha = {props.AlternaCampoSenha}
                    /> : 
                    <>
                        <h1>Olá</h1>
                        <p><input type="email" placeholder="E-mail" ref={(c) => this.email = c} required/></p>
                        <p><input type="password" placeholder="Senha" ref={(c) => this.senha = c} required/><label className="1 olhoSenha" id="btnOlhoSenha" onClick={this.ChamaAlternaCampoSenha}>&#128065;</label></p>
                        <p className="linkDireita"><a href="#" onClick={this.ChamaRecuperaSenha}>Esqueci minha senha</a></p>
                        <p><button onClick={this.ValidaForm}>LOGIN</button></p>
                        <p><a className="linkImportante" onClick = {() => {this.AlternaCadastro(true)}}>Cadastrar</a></p>
                    </>
                }
                </form>
                <span id="slotCarregando">
                    <span className="carregando"/>
                </span>
            </div>
        );
    }
}

export default Login;
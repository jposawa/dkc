import React, { Component } from 'react';


class Cadastro extends Component{
    constructor(props){
        super(props);
        //this.ValidaForm = this.ValidaForm.bind(this);
    }

    /* BuscaUsuario(email, senha)
    {
        //console.log(email + " " + senha);
        const parametros = usuario.filter((u) => {return u.email === email && u.senha === senha});
        if(parametros.length === 0)
        {
            alert("E-mail ou senha não conferem");
        }
        else
        {
            //console.log("PARAMETROS: " + parametros[0]);
            this.props.RetornaUsuario(parametros[0].id, email, parametros[0].nvAcesso);
            //this.setState({usuario: parametros[0].id});
            //localStorage.setItem("dolho@User",parametros[0].id);
        }
    } */

    ValidaForm = (event) =>
    {
        this.props.ChamaLoad(true);
        event.preventDefault();
        let email = this.email.value;
        let senha = this.senha.value;
        let confirmaSenha = this.confirmaSenha.value;

        if(email === null || email === "")
        {
            alert("O campo de e-mail precisa ser preenchido");
            this.email.focus();
            this.props.ChamaLoad();
        }
        else if(email.split("@")[1] === "" || email.split("@").length < 2)
        {
            alert("O campo de e-mail está inválido");
            this.email.focus();
            this.props.ChamaLoad();
        }
        else if(senha === null || senha === "")
        {
            alert("O campo de senha precisa ser preenchido");
            this.senha.focus();
            this.props.ChamaLoad();
        }
        else if(confirmaSenha === null || confirmaSenha === "")
        {
            alert("É necessário confirmar a senha");
            this.confirmaSenha.focus();
            this.props.ChamaLoad();
        }
        else if(confirmaSenha !== senha)
        {
            alert("Os campos de Senha e Confirmação de senha precisam ser iguais");
            this.confirmaSenha.focus();
            this.props.ChamaLoad();
        }
        else
        {
            this.props.CadastroUsuario(email, senha);
            //this.BuscaUsuario(email, senha);
        }
        this.props.ChamaLoad();
    }

    ChamaAlternaCampoSenha = (botao) =>{
        botao = botao.target;
        let campo;

        if(botao.classList[0] == 1)
        {
            campo = this.senha;
        }
        else if(botao.classList[0] == 2)
        {
            campo = this.confirmaSenha;
        }

        this.props.AlternaCampoSenha(botao, campo);
    }

    render(){
        const {state,props} = this;

        return(
            <>
                <h1>Cadastro</h1>
                <p><input type="email" placeholder="E-mail" ref={(c) => this.email = c} required/></p>
                <p><input type="password" placeholder="Senha" ref={(c) => this.senha = c} required/><label className="1 olhoSenha" onClick={this.ChamaAlternaCampoSenha}>&#128065;</label></p>
                <p><input type="password" placeholder="Confirmar Senha" ref={(c) => this.confirmaSenha = c} required/><label className="2 olhoSenha" onClick={this.ChamaAlternaCampoSenha}>&#128065;</label></p>

                <p><button onClick={this.ValidaForm} className="cadastrar">CADASTRAR</button></p>
                <p><a className="linkImportante" onClick = {() => {props.AlternaCadastro(false)}}>Cancelar cadastro</a></p>
            </>
    );
    }
}

export default Cadastro;
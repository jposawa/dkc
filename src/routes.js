import React, {Component} from 'react';


import {BrowserRouter, Switch, Route} from 'react-router-dom';

//FIREBASE//
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import Login from './pages/login';
import Principal from './pages/principal';

// import stKey, {prefixoChave} from "./stKeys.json";
// import {admin} from "./infos.json";

//import Firebase, {FirebaseContext} from './components/Firebase';

/* import Login from './pages/login';
import Painel from './pages/painel'; */

const prefixoChave = "dkc@";



const config = {
    apiKey: "AIzaSyBu5dKSLhkEcY_NzX_JZ9x4f-uzRh-85ug",
    authDomain: "draenak-26560.firebaseapp.com",
    databaseURL: "https://draenak-26560.firebaseio.com",
    projectId: "draenak-26560",
    storageBucket: "draenak-26560.appspot.com",
    messagingSenderId: "779446277969",
    appId: "1:779446277969:web:524db9c544be1548e86570"
};
//FIREBASE//



class Routes extends Component{
    constructor(props){
        super(props);
  
        this.state={
            usuario:{
                id:null,
                nvAcesso:null,
                token:null,
            },
            config: config,
            passe:null,
        }

        try{
            firebase.initializeApp(config);

            this.auth = firebase.auth();
            this.db = firebase.database();

            
        }
        catch(err){
            if(!/already exists/.test(err.message)){
                console.error("Deu ruim", err.stack);
            }
        }
    }

    ChamaLoad = carregando =>
    {
        // alert(carregando);
        const slotCarregando = document.getElementById("slotCarregando");
        if(carregando)
        {
            slotCarregando.classList.remove("someLoad");
        }
        else
        {
            slotCarregando.classList.add("someLoad");
        }
    }

    LoginEmailSenha = (email, senha) =>{
        //alert("testando");
        try
        {
            this.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(() => {
                this.auth.signInWithEmailAndPassword(email,senha).then(user => {
                    try
                    {
                        /* console.log("user");
                        console.log(this.auth.currentUser.uid); */
                        this.setState({passe:senha});
                        this.RecuperaUsuario(firebase.auth().currentUser.uid, email);
                    }
                    catch(erro)
                    {
                        console.log("ALERTA DE ERRO 2");
                        console.error(erro.message);
                        this.ChamaLoad();
                    }
                }).catch(erro => {
                    console.log("LOGIN FALHOU");
                    console.log(erro);
                    alert("Usuário ou senha inválidos");
                    this.ChamaLoad();
                });
            });
            //this.Usuario();

            //console.log(this.auth.currentUser);
        }
        catch(erro)
        {
            console.log("ALERTA DE ERRO");
            console.log(erro.message);
        }

        /* try
        {
            this.RecuperaUsuario(firebase.auth().currentUser.uid, email);
        }
        catch(erro)
        {
            console.log("ALERTA DE ERRO");
            console.error(erro.message);
        } */
        /* finally
        {
            this.RecuperaUsuario(this.auth.currentUser.uid, email);
        } */
        //console.log(firebase.database());
    }

    CadastroUsuario = (email, senha) =>{
        this.auth.createUserWithEmailAndPassword(email,senha).then(() => {
            alert("Cadastro realizado com sucesso");
        }).catch(erro => {
            console.error(erro);
            if(/already-in-use/.test(erro.code))
            {
                alert("E-mail já cadastrado");
            }
            else
            {
                alert("Erro desconhecido. Favor entrar em contato com o suporte");
            }
            this.ChamaLoad();
        })
    }

    Logout = (confirma) =>{
        // console.log(confirma);
        if(confirma === null || confirma === undefined || confirma === "")
        {
            confirma = window.confirm("Confirma logout?");
        }

        if(confirma)
        {
            this.RetornaUsuario();
            this.auth.signOut();
            // console.log(this.auth.currentUser);
            // this.setState({userFB:undefined});
        }
    }

    SalvaStorage = (chave, valor) =>{
        //alert("chamou o storage")

        if(chave !== null && chave !== undefined)
        {
            chave = prefixoChave + chave;
            if(valor !== null && valor !== undefined)
            {
                localStorage.setItem(chave, valor);
            }
            else
            {
                localStorage.removeItem(chave);
            }
        }
        else
        {
            localStorage.clear();
        }
        //console.log("coisou");
    }

    PegaStorage = chave =>{
        chave = prefixoChave + chave;

        return localStorage.getItem(chave);
    }

    RecuperaUsuario = (idUsuario, email) =>{
        if(idUsuario !== null && idUsuario !== undefined)
        {
            // const parametros = admin.filter((a) => {return a === email});
            let nvAcesso = 1;
            let score = 1;
            /* if(parametros.length > 0)
            {
                nvAcesso = 10;
                score = 110;
            } */
            //const nvAcesso = parametros.length > 0 ? 10 : 1;

            // console.log(idUsuario);

            firebase.database().ref('/usuarios/' + idUsuario).once('value').then(snapshot => {
                // console.log("Retornando isso");
                // console.log(snapshot.val());
                if(snapshot.val() === null)
                {
                    let codigo = idUsuario.substr(0,7).toUpperCase();
                    //console.log("Não existe esse usuário no banco");
                    // const parametros = admin.filter((a) => {return a === email});
                    //console.log(parametros);
                    firebase.database().ref('usuarios/'+idUsuario).set({
                        id:idUsuario,
                        email: email,
                        score: score,
                        nvAcesso: nvAcesso,
                        codigo:codigo,
                        eventos:{
                            realcados:[],
                            cadastrados:[]
                        }
                    });
                }
                else
                {
                    nvAcesso = snapshot.val().nvAcesso;
                    score = snapshot.val().score;
                }
                this.RetornaUsuario(idUsuario,email,nvAcesso,score, true);
            });
        }
        else
        {
            console.error("Rapaz, tem usuário aqui não ó");
        }
    }

    RetornaUsuario = (idUsuario,email,nvAcesso, score, loginFirebase) =>{
        /* console.log("entrou: " + idUsuario);
        console.log(this.auth.currentUser); */

        //ESSA CONDIÇÃO É PARA GARANTIR QUE O ID É UM NÚMERO OU UM TOKEN CRIADO PELO FIREBASE
        if(idUsuario !== null && idUsuario !== undefined && (!isNaN(parseInt(idUsuario) || loginFirebase)))
        {
            idUsuario = !loginFirebase ? parseInt(idUsuario) : idUsuario;
            //console.log("vai chamar");
            this.SalvaStorage("usuario", idUsuario);
            this.SalvaStorage("email", email);
            this.SalvaStorage("nvAcesso", nvAcesso);
            this.SalvaStorage("score", score);
        }
        else
        {
            //console.log("entrou por aqui");
            idUsuario = null;
            nvAcesso = null;
            this.SalvaStorage();
        }
        
        const dadosUsuario = {
            id:idUsuario,
            email:email,
            nvAcesso:nvAcesso,
            score:score
        };

        this.setState({usuario: dadosUsuario, userFB:this.auth.currentUser});
    }

    Usuario = uid => this.db.ref(`usuarios/${uid}`);

    componentDidMount()
    {
        console.log(this.auth.currentUser);
        this.auth.onAuthStateChanged(user => {
            // console.log(user);
            if(user)
            {
                // console.log("Usuário logado:");
                // console.log(user);
                this.db.ref('/usuarios/' + user.uid).once('value').then(snapshot => {
                    const usuario = snapshot.val();
                    let email = user.email;
                    let nvAcesso = 1;
                    let score = 1;

                    if((usuario === null || usuario === undefined) && !user.emailVerified)
                    {
                        this.EnviaVerificaEmail(email);
                        /* user.sendEmailVerification().then(() => {
                            alert("Um e-mail de confirmação foi enviado para você");
                            
                        }).catch((erro) => {
                            console.log(erro.message);
                        }) */
                    }
                    else
                    {
                        nvAcesso = usuario.nvAcesso;
                        score = usuario.score;
                    }

                    const uidUsuario = user.uid;

                    if(uidUsuario !== null && uidUsuario !== undefined)
                    {
                        this.RecuperaUsuario(uidUsuario,email, this.PegaStorage("nvAcesso"), this.PegaStorage("score"), true);
                    }
                    else
                    {
                        this.RecuperaUsuario(uidUsuario, email, nvAcesso, score, true);
                    }
                });
                
                /* this.RetornaUsuario(this.PegaStorage("usuario"),this.PegaStorage("email"), this.PegaStorage("nvAcesso"), this.PegaStorage("score"), true); */
            }
            else
            {
                // console.log("Ta logado não!");
                const formLogin = document.getElementById("formLogin");
                const slotCarregando = document.getElementById("slotCarregando");
                const btnOlhoSenha = document.getElementById("btnOlhoSenha");

                formLogin.classList.add("apareceLogin");
                slotCarregando.classList.add("someLoad");
                btnOlhoSenha.classList.add("apareceOlho")
            }
            this.setState({userFB:user});
        });
        // console.log(this.auth.currentUser);
        /* this.auth.onAuthStateChanged(user => {
            if(user)
            {
                console.log("Usuário logado:");
                console.log(this.auth.currentUser);
                
                this.RetornaUsuario(this.PegaStorage("usuario"),this.PegaStorage("email"), this.PegaStorage("nvAcesso"), true);
            }
            else
            {
                console.log("Ta logado não!");
            }
        }); */
        /* console.log("Usuário logado:");
        console.log(this.auth.currentUser);
        this.RetornaUsuario(this.PegaStorage("usuario"),this.PegaStorage("email"), this.PegaStorage("nvAcesso"), true); */
    }

    RecuperaSenha = email =>{
        this.auth.sendPasswordResetEmail(email).then(() => {
            alert("Um e-mail foi enviado para " + email);
        }).catch(e => {
            // console.log(e);
        });
    }

    AlternaCampoSenha = (botao, campo) =>{
        // console.log(botao);
        // console.log(campo);

        if(!botao.classList.contains("senhaVisivel"))
        {
            botao.classList.add("senhaVisivel");
            campo.type = "text";
        }
        else
        {
            botao.classList.remove("senhaVisivel");
            campo.type = "password";
        }
        
        /* if(campo.classList.length === 1)
        {

        } */
    }

    /*--- USUÁRIO ---*/
    EnviaVerificaEmail(email)
    {
        const usuario = firebase.auth().currentUser;

        // console.log(this);

        if(email === null || email === undefined || email === "")
        {
            email = this.usuario.email;
        }

        // console.log(email);
        usuario.sendEmailVerification().then(() => {
            alert("Link de confirmação enviado para " + email);
        }).catch(erro => {
            console.warn(erro.message);
        });
    }

    AtualizaDados = (dado, info) =>
    {
        let dadoSensivel = false;
        console.log(dadoSensivel);
        switch(dado)
        {
            case "email":
                dadoSensivel = this.AtualizaEmail(info);
            break;

            case "senha":
                dadoSensivel = this.AtualizaSenha(info);
            break;

            default:
                console.warn("Algo de errado não está certo");
            break;
        }
        console.log(dadoSensivel);
        if(dadoSensivel)
        {
            alert("É necessário fazer login novamente");
            this.Logout(true);
        }
    }

    AtualizaEmail = email =>
    {
        const usuario = this.auth.currentUser;
        let sucesso = false;

        if(usuario !== null && usuario !== undefined)
        {
            if(email === null || email === undefined || email === "")
            {
                console.warn("Ei minino, passe o e-mail");
            }
            else
            {
                usuario.updateEmail(email).then(() => {
                    alert("E-mail atualizado com sucesso");
                    this.EnviaVerificaEmail(email);
                    this.ChamaLogoutAutomatico();
                    // sucesso = true;
                }).catch(erro => {
                    console.warn(erro.code);
                    if(/already-in-use/.test(erro.code))
                    {
                        alert("Este e-mail já está sendo utilizado por outro usuário");
                    }
                    else if(/requires-recent-login/.test(erro.code))
                    {
                        alert("Por questões de segurança, é necessário fazer login novamente antes de executar essa operação");
                        this.props.Logout(true);
                    }
                    console.warn(/already-in-use/.test(erro.code));
                });
            }
        }
        else
        {
            console.warn("Não está achando usuário nesta bagaça");
        }

        return sucesso;
    }

    ChamaLogoutAutomatico = () =>
    {
        alert("É necessário fazer login novamente");
        this.props.Logout(true);
    }

    AtualizaSenha = (senhaNova) =>{
        const usuario = this.auth.currentUser;

        let sucesso = false;

        usuario.updatePassword(senhaNova).then((v) => {
            alert("Senha alterada com sucesso");
            this.ChamaLogoutAutomatico();
        }).catch(erro => {
            console.warn(erro);
            console.warn(/requires-recent-login/.test(erro.code));
            if(/requires-recent-login/.test(erro.code))
            {
                alert("Por questões de segurança, é necessário fazer login novamente antes de executar essa operação");
                this.props.Logout(true);
            }
        });
       // console.log(sucesso);
        return sucesso;
    }
    /*--- USUÁRIO ---*/

    
//<FirebaseContext.Provider value={new Firebase()}>
    render(){
        return(
            
            <BrowserRouter>
                <Switch>
                    <Route path='/'>
                    {   
                        this.state.usuario.id === null ? 
                            <Login
                                RetornaUsuario={this.RetornaUsuario}
                                LoginEmailSenha = {this.LoginEmailSenha}
                                RecuperaSenha = {this.RecuperaSenha}
                                CadastroUsuario = {this.CadastroUsuario}
                                ChamaLoad = {this.ChamaLoad}
                                AlternaCampoSenha = {this.AlternaCampoSenha}
                            /> 
                        :
                            <Principal
                                geral = {this.state}
                                Logout = {this.Logout}
                                EnviaVerificaEmail = {this.EnviaVerificaEmail}
                                AtualizaDados = {this.AtualizaDados}
                                AtualizaEmail = {this.AtualizaEmail}
                                AtualizaSenha = {this.AtualizaSenha}
                            />
                    }
                        {/* <Login
                            RetornaUsuario={this.RetornaUsuario}
                            LoginEmailSenha = {this.LoginEmailSenha}
                            RecuperaSenha = {this.RecuperaSenha}
                            CadastroUsuario = {this.CadastroUsuario}
                            ChamaLoad = {this.ChamaLoad}
                            AlternaCampoSenha = {this.AlternaCampoSenha}
                        />:
                        <Painel
                            usuario={this.state.usuario}
                            config = {this.state.config}
                            PegaStorage = {this.PegaStorage}
                            SalvaStorage = {this.SalvaStorage} 
                            RetornaUsuario = {this.RetornaUsuario}
                            Logout = {this.Logout}
                            passe = {this.state.passe}
                            AlternaCampoSenha = {this.AlternaCampoSenha}
                        /> */}
                    </Route>
                </Switch>
            </BrowserRouter>
        );
    }
}


/*

<Route 
                    path="/" 
                    render={(props) => this.state.usuario === null ? <Login {...props} RetornaUsuario={this.RetornaUsuario}/> : <Painel {...props} usuario={this.state.usuario}/>}
                />

<Route exact path='/login'>
                        <Login usuario={this.state.usuario} RetornaUsuario = {this.RetornaUsuario}/>
                    </Route>
                    <Route path='/'>
                        <Painel usuario={this.state.usuario}/>
                    </Route>
                    
                    <Route exact path="/" component={this.state.usuario === null ? Login : Painel} usuario = {this.state.usuario} RetornaUsuario={this.RetornaUsuario}/>
                </Switch>


const Routes = () =>(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={localStorage.getItem(stKey.usuario) === null ? Main : Painel}/>
        </Switch>
    </BrowserRouter>
);


            <Route path="/products/:id" component={Product}/>


const Routes = () =>(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Main}/>
            <Route path="/products/:id" component={Product}/>
        </Switch>
    </BrowserRouter>
);
*/

export default Routes;